﻿//Prepros
#define DEBUGNEIGHBOUR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FluidSimNVIDIA;
using Hashing;

public class NVIDIAPaper : MonoBehaviour {

	const ushort solverIterations = 10;//5;
	const int CUBE_SIZE = 4; //4x4x4
	int particleCount = CUBE_SIZE * CUBE_SIZE * CUBE_SIZE;
	const int PARTICLEABSOLUTELIMIT = 64; // Cannot go higher than this.
	const float MASS = 1.0f; // Uniform mass across all particles.
	const float DENSITY_RESTING = 45.0f; //82
	const float STIFFNESS = 0.08f;
	const float STIFFNESS_APPRX = 0.1f;
	const float SURFACETENSION = 0.0001f;
	const float VISCOSITY_LIN = 0.25f;
	const float VISCOSITY_QUAD = 0.5f;
	const float radius = 0.1f;//0.03f;
	const float smoothingRadius = 6.0f * radius;
	const float smoothingRadiusSq = smoothingRadius * smoothingRadius;
	const float epsilon = 0.0000001f;
	const float epsilonSq = epsilon * epsilon;
	const float KERNEL = 20f / (2f * Mathf.PI * smoothingRadiusSq);     // Smoothing kernels
	const float KERNELNORM = 30f / (2f * Mathf.PI * smoothingRadiusSq);
	static float deltaT = (1 / 60f) / solverIterations;
	static Vector3 Gravity = 9.81f * Vector3.down;
	const float BOUNDARYDIST = 15f;
	const int maxParticlesPerCell = 64;
	const float cellSizeHashgrid = smoothingRadiusSq;//0.75f;
	Particle[] particles;
	ParticleHashNVIDIA hashTable;

	// Global memory alloc
	static List<ushort>[] neighbourhoods;
	static List<float>[] neighbourhoodDistances;
	static Particle p;
	static Vector3[] xlast;
	static Vector3[] xprojected;

	// Incompressible flow simulated via Position Based Dynamics as detailed in the Macklin & M�ller NVIDIA paper
	void Simulate() {
		ushort solves = 0;
		while (solves++ < solverIterations) {

			ApplyExtForces();

			Integrate();

			GenerateNeighbourhoods(solves == solverIterations);

			Pressure();

			Project();

			Correct();

			//hashTable.InsertParticles(particles); // is this even necessary?

			EnforceBoundaries();
		}
	}

	void ApplyExtForces() {
		for (int i = 0; i < particleCount; i++) {
			p = particles[i];
			p.v += (deltaT * GetExternalForcesFor(p));
			particles[i] = p;
		}
	}

	void Integrate() {
		for (int i = 0; i < particleCount; i++) {
			p = particles[i];
			//p.xstar = p.x + (deltaT * p.v);
			xlast[i] = p.x;
			p.x += deltaT * p.v;
			particles[i] = p;
		}
	}

	void GenerateNeighbourhoods(bool drawConnections = false) {
		hashTable.InsertParticles(particles);
		for (int i = 0; i < particleCount; i++) {
			neighbourhoods[i] = new List<ushort>(
				hashTable.NeighboursOfPoint(particles[i].x)
			);
#if DEBUGNEIGHBOUR
			if (drawConnections) {
				foreach (ushort id in neighbourhoods[i]) {
					if (id > i)
						Debug.DrawLine(particles[i].x, particles[id].x, Color.red);
				}
			}
#endif
			neighbourhoodDistances[i] = new List<float>(neighbourhoods[i].Count);
		}
	}

	void Pressure() {
		for (int i = 0; i < particleCount; i++) {
			p = particles[i];
			float density = 0f;
			float densityProjection = 0f;
			for (int n = 0; n < neighbourhoods[i].Count; n++) {
				Particle other = particles[neighbourhoods[i][n]];
				Vector3 dx = other.x - p.x;
				float dist = Vector3.Dot(dx, dx); // SQUARED distance
				if (dist < epsilonSq || dist > smoothingRadiusSq) {   // Other particle is too far away; skip.
					neighbourhoodDistances[i].Add(-1f);     // Just add -1 at n
					continue;
				}

				//NEW: Forced collision depenetration
				//float overlap = Mathf.Min(dist - (2 * radius * radius), 0);
				//p.x += dx * overlap;
				//other.x -= dx * overlap;

				neighbourhoodDistances[i].Add(dist = Mathf.Sqrt(dist)); // Actual distance
				float a = 1f - (dist / smoothingRadius);
				density += MASS * a * a * a * KERNEL;
				densityProjection += MASS * a * a * a * a * KERNELNORM;
			}

			p.d = density;
			p.dv = densityProjection;
			p.p = STIFFNESS * (density - (MASS * DENSITY_RESTING));
			p.pv = STIFFNESS_APPRX * densityProjection;
			particles[i] = p;
		}
	}

	void Project() {
		float deltaTSq = deltaT * deltaT;
		for (int i = 0; i < particleCount; i++) {
			p = particles[i];
			Vector3 xx = p.x;
			for (int n = 0; n < neighbourhoods[i].Count; n++) {

				Particle other = particles[neighbourhoods[i][n]];
				
				float dist = neighbourhoodDistances[i][n];
				if (dist < 0) {
					continue;
				}

				Vector3 dx = other.x - p.x;

				//NEW: Forced collision depenetration
				//float overlap = Mathf.Min(dist - (2 * radius), 0);
				//xx += dx * overlap;

				// Attenuation
				float a = 1f - (dist / smoothingRadius);
				float d = deltaTSq * ((p.pv + other.pv) * a * a * a * KERNELNORM + (p.p + other.p) * a * a * KERNEL) / 2f;

				// Relax w.r.t mass
				xx -= d * dx / (dist * MASS);

				// (The following assumes same materials and phases; TODO extend this if multiphase fluids are explored)
				// Surface tension
				xx += SURFACETENSION * a * a * KERNEL * dx;

				// Viscosity
				Vector3 deltaVel = p.v - other.v;
				float u = Vector3.Dot(deltaVel, dx);
				if (u > 0) {
					u /= dist;
					float I = 0.5f * deltaT * a * (VISCOSITY_LIN * u) + (VISCOSITY_QUAD * u * u);
					xx -= I * dx * deltaT;  // should deltaT be here? becomes deltaT^2
				}
			}

			xprojected[i] = xx;
		}
	}

	void Correct() {
		for (int i = 0; i < particleCount; i++) {
			p = particles[i];
			p.x = xprojected[i];
			p.v = (p.x - xlast[i]) / deltaT;
		}
	}

	Vector3 GetExternalForcesFor(Particle p) {
		return Gravity;
	}
	void EnforceBoundaries() {
		for (int i = 0; i < particleCount; i++) {
			p = particles[i];

			// Old method
			/*for (int d = 0; d < 3; d++) {
				p.x[d] = Mathf.Clamp(p.x[d], -BOUNDARYDIST, BOUNDARYDIST);
			}*/
			// Bouncy
			/*for (int d = 0; d < 3; d++) {
				p.v[d] -= (p.x[d] - Mathf.Clamp(p.x[d], -BOUNDARYDIST, BOUNDARYDIST));
			}*/

			// Works relatively well
			/*Vector3 offset = new Vector3();
			for (int d = 0; d < 3; d++) {
				offset[d] = (p.x[d] - Mathf.Clamp(p.x[d], -BOUNDARYDIST, BOUNDARYDIST));
				p.v[d] -= offset[d];
			}
			p.x -= offset;*/

			// Newest method: Project x forward by v, and CLAMP both position and velocity by that.
			Vector3 overExtension = new Vector3();
			float projectPosAxis;
			for (int d = 0; d < 3; d++) {
				projectPosAxis = p.x[d] + (p.v[d] * deltaT);// - Mathf.Clamp(p.x[d], -BOUNDARYDIST, BOUNDARYDIST));
				overExtension[d] = projectPosAxis - Mathf.Clamp(projectPosAxis, -BOUNDARYDIST, BOUNDARYDIST);
			}
			p.x -= overExtension;
			p.v -= overExtension / deltaT;

			particles[i] = p;
		}
	}

	void InstanceParticles() {
		particles = new Particle[particleCount];
		xlast = new Vector3[particleCount];
		xprojected = new Vector3[particleCount];
		neighbourhoods = new List<ushort>[particleCount];
		neighbourhoodDistances = new List<float>[particleCount];

		visualParticles = new Transform[PARTICLEABSOLUTELIMIT];
		hashTable = new ParticleHashNVIDIA(cellSizeHashgrid, maxParticlesPerCell, PARTICLEABSOLUTELIMIT);

		for (int k = 0; k < PARTICLEABSOLUTELIMIT; k++) {
			visualParticles[k] = Instantiate(visualPrefab, transform).transform;
			visualParticles[k].localPosition = Vector3.zero;
			visualParticles[k].localScale = Vector3.one * visualScale;

			// Disable unused particles for now.
			if (k >= particleCount)
				visualParticles[k].gameObject.SetActive(false);
		}

		//for (int i = 0; i < particleCount; i++) {
		ushort i = 0;
		for (int z = 0; z < CUBE_SIZE; z++)
			for (int y = 0; y < CUBE_SIZE; y++)
				for (int x = 0; x < CUBE_SIZE; x++) {
					particles[i] = new Particle {
						id = i++,
						x = new Vector3(x,y,z)*radius,//new Vector3(x + Random.value, y + Random.value, z + Random.value) * radius,
						v = Vector3.zero //v = Random.insideUnitSphere
					};
				}
	}

	// My blackened hatred for Unity cannot truly be expressed in any human tongue.
	void DrawVis() {
		/*foreach (Particle p in particles) {
			Debug.DrawLine(transform.position + p.x + Vector3.left, transform.position + p.x + Vector3.right);
			Debug.DrawLine(transform.position + p.x + Vector3.up, transform.position + p.x + Vector3.down);
			Debug.DrawLine(transform.position + p.x + Vector3.forward, transform.position + p.x + Vector3.back);
		}*/
		for (int i = 0; i < particleCount; i++) {
			visualParticles[i].localPosition = particles[i].x;
		}
	}


	// For the (admittedly suboptimal and slow) visualisation of the particles, create a sphere at each point.
	// TODO when geometry shaders start behaving, move to that.
	[SerializeField]
	private GameObject visualPrefab;
	private Transform[] visualParticles;
	[SerializeField]
	private float visualScale = 0.1f;
	[SerializeField]
	private Text textFieldDebug;

	private void Start() {
		InstanceParticles();
		deltaT = Time.fixedDeltaTime / solverIterations;
	}

	private void FixedUpdate() {
		Simulate();
		DrawVis();
		textFieldDebug.text = "Particle #" + 5 + ": "
			+ particles[5].x + ", "
			+ particles[5].v + ", "
			+ particles[5].d + ", "
			+ particles[5].p + ". ";
		Debug.DrawRay(particles[5].x, Vector3.up, Color.yellow);
	}

	private void Update() {
		if (Input.GetKeyDown(KeyCode.G)) {
			for (int i = 0; i < particleCount; i++) {
				//particles[i].v += Random.insideUnitSphere;
				particles[i].v += (-particles[i].x).normalized + Random.insideUnitSphere / 2;
			}
		}
	}
}
