﻿namespace NearestNeighbor
{

    public struct Uint2
    {
        public uint x;
        public uint y;
    }


    public struct Uint3
    {
        public uint x;
        public uint y;
        public uint z;

    }

}