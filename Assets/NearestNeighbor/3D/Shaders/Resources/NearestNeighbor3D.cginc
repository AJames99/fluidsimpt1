
// ---------------------
// Define Data structure (must be same as your particle data)
// ---------------------
// four 32 bit values

struct Data {
	float3 pos;
	uint idx;
};

cbuffer grid {
	float3 gridDim;
	float gridH;
	float offset; // 512
};

StructuredBuffer  <uint2>	_GridIndicesBufferRead;
RWStructuredBuffer<uint2>	_GridIndicesBufferWrite;

// Quick Hack #2 Change here as pos can be negative (+/- Boundary)
float3 GridCalculateCell(float3 pos) {
	return (pos+offset) / gridH;
}

uint GridKey(uint3 xyz) {

	return xyz.x + xyz.y * gridDim.x + xyz.z * gridDim.x * gridDim.y;
}

uint2 MakeKeyValuePair(uint3 xyz, uint value) {
	// uint2([GridHash], [ParticleID]) 
	return uint2(GridKey(xyz), value);	// �t?
}

uint GridGetKey(uint2 pair) {
	return pair.x;
}

uint GridGetValue(uint2 pair) {
	return pair.y;
}

