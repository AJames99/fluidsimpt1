﻿using System.Runtime.InteropServices;
using UnityEngine;
using NearestNeighbor;

namespace NearestNeighborSample.ThreeDimension {
	/// <summary>
	/// Define your data 
	/// </summary>

	public struct SortedParticle {
		public Vector3 pos;
		public uint idx;

		public SortedParticle(Vector3 pos) {
			this.pos = pos;
			idx = 0;
		}
	}

	/// <summary>
	/// Particle system with Grid Optimization
	/// </summary>
	public class SortedParticleSystem {

		#region particles
		public ComputeShader ParticleCS;

		private int threadGroupSize;

		private static readonly int SIMULATION_BLOCK_SIZE = 32;
		private int numParticles;
		#endregion ForParticle

		#region grid

		public Vector3 range = new Vector3(2048, 2048, 2048); // quick hack #1 range is +/- BOUNDARY plus extra 

		public Vector3 gridDim = new Vector3(16, 16, 16);
		GridOptimizer3D<SortedParticle> gridOptimizer;

		#endregion ForGrid

		#region accessors

		public ref ComputeBuffer GetGridIndicesBuffer() {
			return ref gridOptimizer.GetGridIndicesBuffer();
		}

		public int GetParticleNum() {
			return numParticles;
		}

		public float GetGridH() {
			return gridOptimizer.GetGridH();
		}

		#endregion Accessor

		public void Initialise(int numberOfParticles) {
			numParticles = numberOfParticles;
			threadGroupSize = numParticles / SIMULATION_BLOCK_SIZE;
			gridOptimizer = new GridOptimizer3D<SortedParticle>(numParticles, range, gridDim);
		}

		public void Update(ref ComputeBuffer particleBufferIn) {
			gridOptimizer.GridSort(ref particleBufferIn);    // Pass the buffer you want to optimize               
		}

		void Destroy() {
			gridOptimizer.Release();
		}
	}
}