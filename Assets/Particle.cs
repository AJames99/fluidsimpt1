﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

namespace FluidSimNVIDIA {
	public struct Particle {
		public ushort id;
		public Vector3 x;  // Position
//		public Vector3 xstar;   // New position
		public Vector3 v;   // Velocity
		public float d; // Density
		public float dv; // Density change
		public float p; // Pressure
		public float pv; // Pressure change
	}
}

namespace MacklinFluid {
	public class Particle {
		public ushort id;
		public Vector3 position, newPosition, velocity, deltaPos, forces;
		public float lambda;
		public Particle(Vector3 position, ushort id) {
			this.id = id;
			this.position = position;
			newPosition = new Vector3(position.x, position.y, position.z);
			velocity = Vector3.zero;
			lambda = 0;
			deltaPos = Vector3.zero;
			forces = Vector3.zero;
		}
	}
	public struct ParticleGPU {
		public uint id;
		public Vector3 position,newPosition, velocity, deltaPos, forces;
		public float lambda;
		public ParticleGPU(uint id, Vector3 position) {
			this.id = id;
            this.position = position.Copy();
            this.newPosition = position.Copy();
            this.velocity = Vector3.zero;
            this.lambda = 0f;
            this.deltaPos = Vector3.zero;
            this.forces = Vector3.zero;
		}
	}

	public class ParticleVisualiser {
		Vector3[] particlePositions;
		int particleCount, matrixCount;
		Mesh instanceMesh;
		Material instanceMaterial;
		Matrix4x4[][] particleTransforms;
		const int MAXPERMATRIX = 1023; 
		float visualScale;

		public ParticleVisualiser(Vector3[] particleSet, Mesh mesh, Material material, float visualScale) {
			particlePositions = particleSet;
			particleCount = particlePositions.Length;
			instanceMaterial = material;
			instanceMesh = mesh;
			this.visualScale = visualScale;
			AllocateMatrices();
		}

		public void UpdateParticleSet(Vector3[] particleSet) {
			particlePositions = particleSet;
			particleCount = particlePositions.Length;
			AllocateMatrices();
		}

		void AllocateMatrices() {
			matrixCount = (particleCount / MAXPERMATRIX) + 1;

			particleTransforms = new Matrix4x4[matrixCount][];

			int subIndexCount;
			for (int m = 0; m < matrixCount; m++) {
				subIndexCount = Mathf.Min(particleCount - MAXPERMATRIX*m, MAXPERMATRIX);
				particleTransforms[m] = new Matrix4x4[subIndexCount];
				for (int subIndex = 0; subIndex < subIndexCount; subIndex++) {
					particleTransforms[m][subIndex] = new Matrix4x4();
				}
			}
		}

		public void UpdateMatrices() {
			int particleIndex = 0; int subIndexBound;
			for (int m = 0; m < matrixCount; m++) {
				subIndexBound = Mathf.Min(particleCount - MAXPERMATRIX * m, MAXPERMATRIX);
				for (int subIndex = 0; subIndex < subIndexBound; subIndex++) {
					particleTransforms[m][subIndex].SetTRS(particlePositions[particleIndex++], Quaternion.identity, Vector3.one * visualScale);
				}
			}
		}

		public void Draw() {
			for (int m = 0; m < matrixCount; m++) {
				Graphics.DrawMeshInstanced(instanceMesh, 0, instanceMaterial, particleTransforms[m], particleTransforms[m].Length);
			}
		}
	}

	public struct GlobalVars { // Size: 1*int 3*float + 15*float
		public int SOLVER_ITERATIONS;
		public Vector3 GRAVITY, BOUNDARY_DIST;
		public float MASS, DENSITY_RESTING, RADIUS, C, K, N, deltaQ, wDeltaQ, epsilonLambda, H, Hsqr, POLY6, SPIKY, deltaTime;
		public uint PARTICLE_COUNT;
    }

	public static class GlobalVarsSize {
        public const int sizeOf =  sizeof(int) + (sizeof(float) * 3 * 2) + (14 * sizeof(float)) + sizeof(uint);
	}

	public static class Neighbourhood {
		public const int MAXNEIGHBOURS = 100;
	}

	[StructLayout(LayoutKind.Sequential, Pack = 8)]
	public struct NeighbourhoodGPU {
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = Neighbourhood.MAXNEIGHBOURS)]
		public uint[] ids;
		public int count;
		public NeighbourhoodGPU(uint[] neighbours) {
			count = neighbours.Length;
			ids = neighbours; System.Array.Resize<uint>(ref ids, Neighbourhood.MAXNEIGHBOURS);
		}
	}

	public static class Kernel {
		static public float H, Hsqr, Hcube;
		static public float POLY6, SPIKY;
		public static void SetKernel(float interactionRadiusH) {
			H = interactionRadiusH;
			Hsqr = H * H;
			Hcube = H * H * H;
			// Smoothing kernels:
			// For density estimation
			POLY6 = (315.0f / (64.0f * Mathf.PI * Mathf.Pow(H, 9)));
			// For gradient calculation
			SPIKY = (-45.0f / (Mathf.PI * Mathf.Pow(H, 6)));
		}
		public static float Poly6Kernel(Vector3 pi, Vector3 pj) {
			Vector3 r = pi - pj;
			float len2 = r.sqrMagnitude;
			if (len2 > Hsqr || len2 <= 0) {
				return 0f;
			} else {
				return (POLY6 * Mathf.Pow(Hsqr - r.magnitude, 3f));
			}
		}

		public static Vector3 SpikyGradient(Vector3 pi, Vector3 pj) {
			Vector3 r = pi - pj;
			float len2 = r.sqrMagnitude;
			if (len2 > Hsqr || len2 <= 0) {
				return Vector3.zero;
			} else {
				float len = r.magnitude;
				r.Normalize();
				float term = (H - len) * (H - len) * SPIKY;
				return r * term;
			}
		}
	}
}
