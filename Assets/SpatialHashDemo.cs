﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpatialHashDemo : MonoBehaviour {

	class IndexGrid {
		public List<int>[,] indices;
		public float cellSize;
		public int cellCount;
		float boundaryHalf;
		public IndexGrid(int cellCount, float boundaryHalf) {
			this.cellCount = cellCount;
			this.boundaryHalf = boundaryHalf;

			indices = new List<int>[cellCount, cellCount];
			cellSize = (2 * boundaryHalf) / cellCount;
			for (int x = 0; x < cellCount; x++)
				for (int y = 0; y < cellCount; y++)
					indices[x, y] = new List<int>();
		}

		public void InsertObjects(Transform[] objects) {
			for (int i = 0; i < objects.Length; i++) {
				int x, y;
				x = (int)((objects[i].localPosition.x + boundaryHalf) / cellSize);
				y = (int)((objects[i].localPosition.y + boundaryHalf) / cellSize);
				Debug.Log(i + ": " + "x: " + x + ", y: " + y);
				//int index = x + (cellCount * y);
				indices[x, y].Add(i);
			}
		}

		public int[] NeighboursOf(Vector3 position, int subject) {
			List<int> neighbours = new List<int>();
			int x, y;
			x = (int)((position.x + boundaryHalf) / cellSize);
			y = (int)((position.y + boundaryHalf) / cellSize);
			Debug.Log(subject + ": " + "x: " + x + ", y: " + y);

			for (int i = -1; i < 1; i++) {
				for (int j = -1; j < 1; j++) {

					if (x + i < 0 || y + j < 0 || x + i >= cellCount || y + j >= cellCount)
						continue;

					int[] inCell = indices[x + i, y + j].ToArray();

					foreach (int cellMember in inCell)
						if (cellMember != subject)
							neighbours.Add(cellMember);

				}
			}

			return neighbours.ToArray();
		}
	}

	class HashTableGrid {
		Dictionary<Vector2, List<Transform>> hash = new Dictionary<Vector2, List<Transform>>();
		Dictionary<Vector2, Color> cellColours = new Dictionary<Vector2, Color>();
		int cellSize;
		public HashTableGrid(int cellSize) {
			this.cellSize = cellSize;
			// Pre-declare the number of cells in the dictionary to preallocate the memory.
			hash = new Dictionary<Vector2, List<Transform>>(256);
		}

		public Vector2 PositionToKey(Vector3 position) {
			int x, y;
			x = Mathf.RoundToInt(position.x / cellSize) * cellSize;
			y = Mathf.RoundToInt(position.y / cellSize) * cellSize;
			return new Vector2(x, y);
		}

		public void InsertObjects(Transform[] objects) {
			
			// Clear the hash dict, but keep the cell colours for consistency's sake
			hash.Clear();

			for (int i = 0; i < objects.Length; i++) {
				Vector2 key = PositionToKey(objects[i].localPosition);

				if (!hash.ContainsKey(key)) {
					hash.Add(key, new List<Transform>() { objects[i] });
					if(!cellColours.ContainsKey(key))
						cellColours.Add(key, new Color(Random.value, Random.value, Random.value));
				} else {
					hash[key].Add(objects[i]);
				}
				//Debug.Log(objects[i].name + " at " + objects[i].localPosition + " added at key " + key);
			}
		}

		public void ColouriseObjects(Transform[] objects) {
			for (int i = 0; i < objects.Length; i++) {
				Vector2 key = PositionToKey(objects[i].localPosition);
				Color cellColour = cellColours[key];
				objects[i].GetComponent<MeshRenderer>().material.color = cellColour;
			}
		}

		public Transform[] NeighboursOfObject(Transform subject) {
			List<Transform> neighbours = new List<Transform>();
			List<Transform> atCell;
			Vector2 key = PositionToKey(subject.localPosition);
			for (int i = -1; i <= 1; i += 1) {
				for (int j = -1; j <= 1; j += 1) {
					//Vector2 neighborSample = PositionToKey(subject.localPosition + (Vector3.right * i) + (Vector3.up * j));
					Vector2 neighbourKey = new Vector2(key.x + (i * cellSize), key.y + (j * cellSize));
					if (hash.ContainsKey(neighbourKey))
						atCell = hash[neighbourKey];
					else
						continue;

					if (atCell.Count > 0)
						neighbours.AddRange(atCell);
				}
			}
			/*
						if (hash.ContainsKey(key)) {
							if (hash[key].Count > 0)
								neighbours.AddRange(hash[key]);
						}
			*/

			//Debug.Log(subject + ": " + "x: " + x + ", y: " + y);
			return neighbours.ToArray();// hash[new Vector2(x, y)].ToArray();
		}

		public Color CellColour(Vector3 position) {
			Vector2 key = PositionToKey(position);
			if (cellColours.ContainsKey(key))
				return cellColours[key];
			else
				return Color.black;
		}

		Vector2[][] squareSides = new Vector2[][] {
			new Vector2[] { new Vector2(-1,-1), new Vector2(-1,1) },
			new Vector2[] { new Vector2(1,-1), new Vector2(1,1) },
			new Vector2[] { new Vector2(-1,-1), new Vector2(1,-1) },
			new Vector2[] { new Vector2(-1,1), new Vector2(1,1) },
		};

		public void DrawCells() {
			foreach (Vector2 key in hash.Keys) {
				Vector3 centre = new Vector3(key.x, key.y, 0f);
				float span = cellSize;
				foreach (Vector2[] pair in squareSides) {
					Debug.DrawLine(
						new Vector3(pair[0].x + key.x, pair[0].y + key.y, 0f) * (cellSize / 2f),
						new Vector3(pair[1].x + key.x, pair[1].y + key.y, 0f) * (cellSize / 2f),
						cellColours[key]
					);
				}
			}
		}

		public void DrawNeighbours(Transform[] objects, Dictionary<Transform, int> id) {
			for (int i = 0; i < objects.Length; i++) {
				Transform[] neighbours = NeighboursOfObject(objects[i]);
				if (neighbours.Length > 0) {
					foreach (Transform neighbour in neighbours) {
						// Only draw lines from objects to neighbours where the object has a smaller ID
						// This prevents bi-directionality and drawing lines to self
						if (id[neighbour] > id[objects[i]]) {
							Debug.DrawLine(objects[i].position, neighbour.position, CellColour(neighbour.position));
						}
					}
				}
			}
		}
	}

	[SerializeField]
	GameObject objectPrefab;

	[SerializeField]
	int numObjects = 16;

	Transform[] objects;

	[SerializeField]
	float boundaryHalf = 10f;

	IndexGrid spatialGrid;
	HashTableGrid hashGrid;

	Dictionary<Transform, int> idMapping;

	// Start is called before the first frame update
	void Start() {
		objects = new Transform[numObjects];
		idMapping = new Dictionary<Transform, int>(numObjects);
		for (int i = 0; i < numObjects; i++) {
			objects[i] = Instantiate(objectPrefab, transform).transform;
			TextMeshProUGUI text = objects[i].GetComponentInChildren<TextMeshProUGUI>();
			text.text = i.ToString();
			objects[i].name = "Object #" + i;
			idMapping.Add(objects[i], i);
		}
		RandomlyPositionObjects2D();

		/*
		spatialGrid = new IndexGrid(20, boundaryHalf);
		spatialGrid.InsertObjects(objects);

		for (int i = 0; i < spatialGrid.cellCount; i++) {
			for (int j = 0; j < spatialGrid.cellCount; j++) {

				if (spatialGrid.indices[i, j].Count > 0) {
					string indices = "";
					foreach (int index in spatialGrid.indices[i, j]) {
						indices += index + ", ";
					}
					indices = indices.Substring(0, indices.Length - 2);
					Debug.Log("Grid cell " + i + "," + j + ": " + indices);
				}

			}
		}

		for (int c = 0; c < 5; c++) {
			int michael = Random.Range(0, numObjects - 1);
			string neighbourTest = michael + "'s neighbours are: ";
			int[] michaelsNeighbours = spatialGrid.NeighboursOf(objects[michael].localPosition, michael);
			if (michaelsNeighbours.Length > 0) {
				foreach (int neighbour in michaelsNeighbours) {
					neighbourTest += neighbour + ", ";
				}

				neighbourTest = neighbourTest.Substring(0, neighbourTest.Length - 2);
			} else {
				neighbourTest = michael + " has no neighbours. :(";
			}
			Debug.Log(neighbourTest);
		}
		*/

		hashGrid = new HashTableGrid(2);
		hashGrid.InsertObjects(objects);

		/*for (int anderson = 0; anderson < numObjects; anderson++) {
			string neighbourTest = anderson + "'s neighbours are: ";
			Transform[] andersonsNeighbours = hashGrid.NeighboursOfObject(objects[anderson]);
			if (andersonsNeighbours.Length > 0) {
				foreach (Transform neighbour in andersonsNeighbours) {
					if (neighbour != objects[anderson]) {
						Debug.DrawLine(objects[anderson].position, neighbour.position, hashGrid.CellColour(neighbour.position));
						neighbourTest += neighbour.name + ", ";
					}
				}

				neighbourTest = neighbourTest.Substring(0, neighbourTest.Length - 2);
			} else {
				neighbourTest = anderson + " has no neighbours. :(";
			}
			Debug.Log(neighbourTest);
		}*/

		hashGrid.DrawNeighbours(objects, idMapping);

		hashGrid.DrawCells();

		hashGrid.ColouriseObjects(objects);
	}

	Transform objTemp;
	const float moveAmount = 3.0f; float sinT, cosT;
	void RandomlyShiftObjects(float dt) {
		sinT = Mathf.Sin(Time.time);
		cosT = Mathf.Sin(Time.time);
		for (int i = 0; i < numObjects; i++) {
			objTemp = objects[i];
			objTemp.localPosition += new Vector3(
				Mathf.PerlinNoise(sinT + objTemp.localPosition.x + i, sinT + objTemp.localPosition.y + i) - 0.5f,
				Mathf.PerlinNoise(sinT + objTemp.localPosition.x + 2 + i, sinT + objTemp.localPosition.y + 3 + i) - 0.5f,
				0f
			) * dt * moveAmount;
		}
	}

	void RandomlyPositionObjects2D() {
		for (int i = 0; i < numObjects; i++) {
			objects[i].localPosition = new Vector3(Random.Range(-boundaryHalf, boundaryHalf), Random.Range(-boundaryHalf, boundaryHalf), 0f);
		}
	}

	private void Update() {
		RandomlyShiftObjects(Time.deltaTime);
		hashGrid.InsertObjects(objects);
		hashGrid.DrawNeighbours(objects, idMapping);
		hashGrid.DrawCells();
		hashGrid.ColouriseObjects(objects);
	}
}
