﻿using UnityEngine;
using System.Collections;

//It is common to create a class to contain all of your
//extension methods. This class must be static.
public static class ExtensionMethods {
    //Even though they are used like normal methods, extension
    //methods must be declared static. Notice that the first
    //parameter has the 'this' keyword followed by a Transform
    //variable. This variable denotes which class the extension
    //method becomes a part of.
    public static Vector3 Copy(this Vector3 point) {
        return new Vector3(point.x, point.y, point.z);
    }

    public static float Squared(this float value) {
        return value * value;
    }

    public static int Squared(this int value) {
        return value * value;
    }

    // Packs unsigned shorts (16bit) into unsigned ints (32bit)
    public static uint[] PackInto(this ushort[] source) {
        uint[] packedArr = new uint[Mathf.CeilToInt(source.Length / 2f)];
		for (int i = 0; i < source.Length; i++) {
            uint a = (uint)source[i];
            a <<= 16 * (i % 2);
            packedArr[i / 2] += a;
        }
        return packedArr;
    }
}