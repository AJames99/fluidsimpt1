//Prepros
//#define DEBUGNEIGHBOUR
//#define COLLISIONDETECTION
//#define SPEEDLIMIT
#define SWIRLYGRAVITY
#define NOISYSPAWN
#define USEFIXEDUPDATE
//#define USEHASHMAP
//#define CPUNEIGHBOURS
//#define NODRAW
//#define PACKUSHORT
//#define INSTANCEDRAW

#define LocalNeighbourSearch

using System.Runtime.InteropServices;

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.Universal;
using NearestNeighborSample.ThreeDimension;

namespace MacklinFluid {
	public class MacklinFluidGPU : MonoBehaviour {

		const ushort solverIterations = 5;//3;
        const int CUBE_SIZE = 32;//12; //4x4x4 // powers of 2
		int particleCount = CUBE_SIZE * CUBE_SIZE * CUBE_SIZE;
		const int PARTICLEABSOLUTELIMIT = CUBE_SIZE* CUBE_SIZE* CUBE_SIZE; // Cannot go higher than this.
		const float MASS = 1.0f; // Uniform mass across all particles.
		[SerializeField, Range(0.00001f, 0.01f)]
		float DENSITY_RESTING = 0.00019f; // 0.002 works
		const float RADIUS = 15; // 15 works
		const float C = 0.01f; // XSPH Viscosity, default 0.01
		const float K = 0.1f;
		const float N = 4f; // default 4
		const float deltaQ = 0.2f * RADIUS;
		float wDeltaQ;
		[SerializeField, Range(0f, 15.0f)]
		float epsilonLambda = 0.15f;
		[SerializeField]
		Vector3 Gravity = new Vector3(0f, -9.81f, 0f);
		[SerializeField]
		Vector3 BOUNDARYDIST = new Vector3(400f, 400f, 400f);
		const int maxParticlesPerCell = 16;
		const float cellSizeHashgrid = RADIUS;
		ParticleGPU[] particles;

        SortedParticle[] particlePositions;

        SortedParticleSystem particleGridSort;


       [SerializeField]
		Transform[] walls;
		[SerializeField] ReflectionProbe mainProbe;
#if CPUNEIGHBOURS
#if USEHASHMAP
		ParticleHashGPU hashTable;
#else
		NeighbourhoodLookupCPU neighbourLookup;
#endif
#endif

		[SerializeField]
		ComputeShader computeShader;
#if !CPUNEIGHBOURS
		int kernelNeighbours;

#endif
		ComputeBuffer particlesBuffer, particlePositionsBuffer, globalVarsBuffer, neighboursBuffer, neighbourCountsBuffer;
		int kernelForces, kernelDensity, kernelPressure, kernelIntegrate, kernelViscosity;
		uint threadGroupSize;

		// Global memory alloc
		uint[] neighbours;
		uint[] neighbourCounts;
		GlobalVars[] globalVars;

#if INSTANCEDRAW
		ParticleVisualiser particleVisualiser;
		[SerializeField]
		Mesh instanceMesh;
		[SerializeField]
		Material instanceMat;
#else
		[SerializeField]
		Material visMaterial;
		[SerializeField]
		Material visMaterialNrm;

		[SerializeField] ForwardRendererData rendererData;
		FluidRenderFeature fluidRenderFeature;

//		[SerializeField]
//		int layerNrm;
		int[] args;
		ComputeBuffer argsBuffer;
		Bounds drawBounds;
		RenderBuffer[] MRTs;
#endif

		[SerializeField]
		private float visualScale = 0.1f;
		private Vector3 visualScaleV;
		[SerializeField]
		private Text textFieldDebug;

		private void Start() {
			InitParticles();

			visualScaleV = new Vector3(visualScale, visualScale, visualScale);

			Physics.gravity = Gravity;
		}

#if USEFIXEDUPDATE
		private void FixedUpdate() {
			Step();
		}
#endif

		void Step() {
			// Pack uniform-constant Buffers --------------------

#if SWIRLYGRAVITY
			Vector3 G = new Vector3(30 * Mathf.Cos(Time.fixedTime / 2f), -800, 5 * Mathf.Cos(Time.fixedTime / 10f));
			//G.y += G.x + G.z;
			globalVars[0].GRAVITY = G;
			globalVarsBuffer.SetData(globalVars); // Binds array of struct
			computeShader.SetConstantBuffer("globalVars", globalVarsBuffer, 0, GlobalVarsSize.sizeOf);
#endif

#if CPUNEIGHBOURS
			neighboursBuffer.SetData(neighbours);
			neighbourCountsBuffer.SetData(neighbourCounts);
#endif
			// Main Loop -----------------------
			int threadGroups = (int)((particleCount + (threadGroupSize - 1)) / threadGroupSize);

			DoKernel(kernelForces, threadGroups);

            // CPU Computation -----------------
#if CPUNEIGHBOURS
			GenerateNeighbourhoods(true);
#else

            //sort the particles and generate the grid lookups
            particleGridSort.Update(ref particlePositionsBuffer);

            computeShader.SetBuffer(kernelNeighbours,"_GridIndicesBufferRead", particleGridSort.GetGridIndicesBuffer());

            DoKernel(kernelNeighbours, threadGroups);
#endif
            int iterations = 0;
			while (iterations++ < solverIterations) {
				DoKernel(kernelDensity, threadGroups);
				DoKernel(kernelPressure, threadGroups);
				DoKernel(kernelIntegrate, threadGroups);
			}

            DoKernel(kernelViscosity, threadGroups);

#if INSTANCEDRAW
			particlePositionsBuffer.GetData(particlePositions);
#endif
        }

        private void DoKernel(int kernel, int threadGroups) {
			// Bind data -----------------------
			BindData(kernel);
			// Execute
			computeShader.Dispatch(kernel, threadGroups, 1, 1);
		}

		private void BindData(int kernel) {
			computeShader.SetBuffer(kernel, "particles", particlesBuffer);

            computeShader.SetVector("gridDim", particleGridSort.gridDim);
            computeShader.SetFloat("gridH", particleGridSort.GetGridH());
            computeShader.SetFloat("offset", 1024);

            computeShader.SetBuffer(kernel, "particlePositions", particlePositionsBuffer);

            computeShader.SetBuffer(kernel, "neighbours", neighboursBuffer);
			computeShader.SetBuffer(kernel, "neighbourCounts", neighbourCountsBuffer);
		}

		private void Update() {
#if !USEFIXEDUPDATE
			globalVars[0].deltaTime = Time.deltaTime;
			globalVarsBuffer.SetData(globalVars);
			Step();
#endif
#if !NODRAW && INSTACEDRAW
			DrawVis();
#endif
			fluidRenderFeature.SetLights(Light.GetLights(LightType.Directional, 0));
		}

		private void OnDisable() {
			particlesBuffer.Dispose();
			particlesBuffer.Release();
			particlePositionsBuffer.Dispose();
			particlePositionsBuffer.Release();
			globalVarsBuffer.Dispose();
			globalVarsBuffer.Release();
			neighbourCountsBuffer.Dispose();
			neighbourCountsBuffer.Release();
			neighboursBuffer.Dispose();
			neighboursBuffer.Release();
			argsBuffer.Dispose();
			argsBuffer.Release();
			fluidRenderFeature.settings.IsEnabled = false;
			MRTs = null;
		}

		private void OnDestroy() {
			OnDisable();
		}

		void InitParticles() {

            particles = new ParticleGPU[particleCount];
			particlePositions = new SortedParticle[particleCount];

            particleGridSort = new SortedParticleSystem();
            particleGridSort.Initialise(particleCount);

            neighbours = new uint[particleCount *  Neighbourhood.MAXNEIGHBOURS];
			neighbourCounts = new uint[particleCount];
#if USEHASHMAP
			hashTable = new ParticleHashGPU(cellSizeHashgrid, maxParticlesPerCell, PARTICLEABSOLUTELIMIT);
#else
			//neighbourLookup = new NeighbourhoodLookupCPU(particlePositions, RADIUS*2);
#endif
            
			kernelForces = computeShader.FindKernel("Forces");
			kernelDensity = computeShader.FindKernel("Density");
			kernelPressure = computeShader.FindKernel("Pressure");
			kernelIntegrate = computeShader.FindKernel("UpdatePosition");
			kernelViscosity = computeShader.FindKernel("Viscosity");

            kernelNeighbours = computeShader.FindKernel("GenerateNeighbours");

            // Should be the same across all
            computeShader.GetKernelThreadGroupSizes(kernelForces, out threadGroupSize, out _, out _);

			// int ID, 5 vector3s, 1 float 
			int stride = Marshal.SizeOf(typeof(ParticleGPU));

            particlesBuffer = new ComputeBuffer(particleCount, stride);
			particlePositionsBuffer = new ComputeBuffer(particleCount, Marshal.SizeOf(typeof(SortedParticle)));
			neighbourCountsBuffer = new ComputeBuffer(particleCount, sizeof(uint));
			neighboursBuffer = new ComputeBuffer(particleCount * Neighbourhood.MAXNEIGHBOURS, sizeof(uint));
			globalVarsBuffer = new ComputeBuffer(1, GlobalVarsSize.sizeOf, ComputeBufferType.Constant, ComputeBufferMode.Immutable);

			Kernel.SetKernel(RADIUS);
			wDeltaQ = Kernel.POLY6 * Mathf.Pow(((RADIUS * RADIUS) - (deltaQ * deltaQ)), 3);

            globalVars = new GlobalVars[] {
                new GlobalVars {
                    SOLVER_ITERATIONS = solverIterations,
                    GRAVITY = Gravity,
                    MASS = MASS,
                    DENSITY_RESTING = DENSITY_RESTING,
                    RADIUS = RADIUS,
                    C = C,
                    K = K,
                    N = N,
                    deltaQ = deltaQ,
                    wDeltaQ = wDeltaQ,
                    epsilonLambda = epsilonLambda,
                    H = Kernel.H,
                    Hsqr = Kernel.Hsqr,
                    POLY6 = Kernel.POLY6,
                    SPIKY = Kernel.SPIKY,
                    BOUNDARY_DIST = BOUNDARYDIST,
                    deltaTime = Time.fixedDeltaTime,
                    PARTICLE_COUNT = (uint) particleCount
                }
			};

			globalVarsBuffer.SetData(globalVars); // Binds array of struct
			computeShader.SetConstantBuffer("globalVars", globalVarsBuffer, 0, GlobalVarsSize.sizeOf);


            // cube size of 32 and radius of 15 gives us points between 0 and 480
            // so to centre...   which gives us -240 to + 240 
			ushort i = 0;
            int crange  = CUBE_SIZE / 2;

            // create the particles centred around 0,0,0
            for (int z = -crange; z < crange; z++)
				for (int y = -crange; y < crange; y++)
					for (int x = -crange; x < crange; x++) {
#if NOISYSPAWN
                        particlePositions[i].pos = (new Vector3(x, y, z) * RADIUS - (Random.insideUnitSphere * (10f / RADIUS)));
                        //new Vector3(-BOUNDARYDIST, -BOUNDARYDIST, -BOUNDARYDIST) / 2f;

                        particlePositions[i].idx = i;
                        particles[i] = new ParticleGPU(i, particlePositions[i].pos);
                        i++;
#else
						particles[i] = new Particle(new Vector3(x, y, z) * RADIUS, i++);
#endif
					}
           //

            particlesBuffer.SetData(particles);
			particlePositionsBuffer.SetData(particlePositions);

#if INSTANCEDRAW
			particleVisualiser = new ParticleVisualiser(particlePositions, instanceMesh, instanceMat, visualScale);
#else
			// Bind buffer to material
			//visMaterial.SetBuffer("particlePositions", particlePositionsBuffer);
			//visMaterialNrm.SetBuffer("particlePositions", particlePositionsBuffer);
			// New: Bind buffer to scriptable render pass material
			fluidRenderFeature = rendererData.rendererFeatures.OfType<FluidRenderFeature>().FirstOrDefault();
			fluidRenderFeature.SetBuffer("particlePositions", particlePositionsBuffer);

			//Args for indirect draw
			args = new int[]
			{
				(int)1, //vertex count per instance // should change -> 4?
				(int)particleCount, //instance count
				(int)0, //start index location
				(int)0, //base vertex location
				(int)0 //start instance location
			};
			argsBuffer = new ComputeBuffer(args.Length, sizeof(int), ComputeBufferType.IndirectArguments);
			argsBuffer.SetData(args);

			fluidRenderFeature.argsBuffer = argsBuffer;
			drawBounds = new Bounds(Vector3.zero, Vector3.one * BOUNDARYDIST.x * 2);
			//fluidRenderFeature.drawBounds = drawBounds;
			fluidRenderFeature.transform = transform; // CommandBuffer needs a Matrix4x4 for some reason so pass this I guess?

			fluidRenderFeature.settings.IsEnabled = true;
			rendererData.SetDirty(); // Flag the renderer data as dirty now we've changed it

			fluidRenderFeature.SetReflectionProbe(mainProbe);
#endif
		}
	}
}