Shader "Custom/NormalFinal" {
    Properties
    {
		uSampleOffset("Sample Offset", Int) = 4
    }
    SubShader {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"
			#include "../FloatEncodeExtension.cginc"

            sampler2D _InitialParticle2nd;
			float4 _InitialParticle2nd_TexelSize;
			sampler2D _FrameBufferTex;

			#define AverageNormalSampler _FrameBufferTex
			#define DepthSampler _InitialParticle2nd
			#define DepthSampler_TexelSize _InitialParticle2nd_TexelSize
			#define ScreenSize _InitialParticle2nd_TexelSize.zw

			uint uSampleOffset;

			float3 RGBToNormal(float3 vNormal) {
				return (vNormal * 2) - 1;
			}

			float3 NormalToRGB(float3 vNormal) {
				return (vNormal + 1) / 2;
			}

            fixed4 frag (v2f_img i) : SV_Target {
                float2 screenPos = DepthSampler_TexelSize.zw * i.uv;
				float fd = DecodeFloatRGB(tex2D(DepthSampler, i.uv));
				float3 Normal = float3(0,0,0);

				if ((int(screenPos.x) % uSampleOffset == 0) && (int(screenPos.y) % uSampleOffset == 0)) {
					Normal = normalize(tex2D(AverageNormalSampler, i.uv).xyz);
				} else {
					if(fd > 0) {
						int GridX = int(floor(screenPos.x / uSampleOffset) * uSampleOffset);
						int GridY = int(floor(screenPos.y / uSampleOffset) * uSampleOffset);

						float DistPercentX = (screenPos.x - GridX) * 1.0 / uSampleOffset;
						float DistPercentY = (screenPos.y - GridY) * 1.0 / uSampleOffset;

						float3 SampleNormal[4];
						SampleNormal[0] = float3(0, 0, 0);
						SampleNormal[1] = float3(0, 0, 0);
						SampleNormal[2] = float3(0, 0, 0);
						SampleNormal[3] = float3(0, 0, 0);

						float2 TexCoord0 = float2(GridX, GridY) / ScreenSize;
						float3 tNormal = tex2D(AverageNormalSampler, TexCoord0).xyz;
						SampleNormal[0] = RGBToNormal(tNormal);

						if ((screenPos.x+1 < ScreenSize.x) && (GridX+uSampleOffset < ScreenSize.x)) {
							float2 TexCoord1 = float2(GridX+uSampleOffset, GridY) / ScreenSize;
							tNormal = tex2D(AverageNormalSampler, TexCoord1).xyz;
							SampleNormal[1] = RGBToNormal(tNormal);
						}

						if ((screenPos.y+1 < ScreenSize.y) && (GridY+uSampleOffset < ScreenSize.y)) {
							float2 TexCoord2 = float2(GridX, GridY+uSampleOffset) / ScreenSize;
							tNormal = tex2D(AverageNormalSampler, TexCoord2).xyz;
							SampleNormal[2] = RGBToNormal(tNormal);
						}

						if ((screenPos.x+1 < ScreenSize.x) && (screenPos.y+1 < ScreenSize.y) && (GridX+uSampleOffset < ScreenSize.x) && (GridY+uSampleOffset < ScreenSize.y)) {
							float2 TexCoord3 = float2(GridX+uSampleOffset, GridY+uSampleOffset) / ScreenSize;
							tNormal = tex2D(AverageNormalSampler, TexCoord3).xyz;
							SampleNormal[3] = RGBToNormal(tNormal);
						}

						float3 Normal01 = SampleNormal[0] * (1 - DistPercentX) + SampleNormal[1] * DistPercentX;
						float3 Normal23 = SampleNormal[2] * (1 - DistPercentX) + SampleNormal[3] * DistPercentY;
						Normal = Normal01 * (1 - DistPercentY) + Normal23 * DistPercentY;
						Normal = normalize(Normal);
					}
				}

				return float4(NormalToRGB(Normal), tex2D(DepthSampler, i.uv).a);
            }
            ENDCG
        }
    }
}
