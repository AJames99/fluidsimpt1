Shader "Custom/AverageNormal" {
    Properties {
        sampleSpread("Sample Spread", Range(1, 20)) = 5
		uDepthFalloff("Depth Falloff", Range(0,50)) = 1
    }
    SubShader {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "../FloatEncodeExtension.cginc"
			#include "UnityCG.cginc"

            sampler2D _InitialParticle;
			sampler2D _CameraDepthTexture;

			float4 _InitialParticle_TexelSize;
			float uProjectFovVertical;
			float uProjectFov;
			float sampleSpread;
			float uDepthFalloff;
			
			float4x4 _InverseView;

			struct v2f_img2
			{
				float4 pos : SV_POSITION;
				half2 uv   : TEXCOORD0;
				float3 ray : TEXCOORD1;

			};


			float4x4 clipToWorld;


			v2f_img2 vert_img2(appdata_img v)
			{
				v2f_img2 o;

				// Render settings
				float far = _ProjectionParams.z;

				float3 vpos = UnityObjectToClipPos(v.vertex);
				
				// Perspective: view space vertex position of the far plane
				float3 rayPers = mul(unity_CameraInvProjection, vpos.xyzz * far).xyz;

			
				o.pos = float4(vpos.x, -vpos.y, 1,1);

				o.uv = v.texcoord;

				o.ray = rayPers;


				return o;
			}



			float2 ClipToScreen(float2 uv) { return uv * _InitialParticle_TexelSize.zw; }
			float2 ScreenToClip(float2 uv) { return uv * _InitialParticle_TexelSize.xy; }

			float SampleDepthRGB(sampler2D samp, float2 uv) {
				return DecodeFloatRGB(tex2D(samp, uv).rgb) * _ProjectionParams.z;
			}

			float dz2(float x, float y, float2 offsetPositive) {
				if(x<=0 || y<=0 || x>=1 || y>=1) {
					return 0;
				}

				float v0 = SampleDepthRGB(_InitialParticle, float2(x, y) - offsetPositive);
				float v1 = SampleDepthRGB(_InitialParticle, float2(x, y));
				float v2 = SampleDepthRGB(_InitialParticle, float2(x, y) + offsetPositive);	
				
				if(v1 == 0) {
					return 0.0;
				} else if((v0 == 0 && v2 != 0)) {
					return (v2 - v1);
				} else if((v2 == 0 && v0 != 0)) {
					return (v1 - v0);
				}
				
				return (v2 - v0) / 2.0;
			}
			
			float dz2x(float x, float y) {
				return dz2(x,y, ScreenToClip(float2(1,0)));
			}

			float dz2y(float x, float y) {
				return dz2(x,y, ScreenToClip(float2(0,1)));
			}


			//Alternative method of normal reconstruction
			float3 reconstructPositionx(in float2 uv, in float z) {
				float2 angleSpacePos = uv - float2(0.5, 0.5);
				float3 viewDirViewSpace = z * normalize(float3(angleSpacePos.x * tan(uProjectFov / 2.0) / 0.5, angleSpacePos.y * tan(uProjectFovVertical / 2.0) / 0.5, 1.0));

				return viewDirViewSpace;
			}


			// Returns view space posution, which is fine as depth was view space.
			float3 reconstructPosition(in float2 uv, in float z) {
								
				float4 position_s = float4(uv * 2.0 - 1.0, z / -1000, 1.0f);
				float4 position_v = mul(UNITY_MATRIX_I_V, position_s);
				return position_v.xyz / position_v.w;
			}


	


			float3 deriveNormal(float2 uv, float depth)	{
				float2 ux = float2(_InitialParticle_TexelSize.x, 0.0);
				float2 uy = float2(0.0, _InitialParticle_TexelSize.y);

				float2 dxPos = uv + ux;		// Right
				float2 dxNeg = uv - ux;		// Left
				float2 dyPos = uv + uy;		// Up
				float2 dyNeg = uv - uy;		// Down

				float depthdxPos = SampleDepthRGB(_InitialParticle, dxPos);
				float depthdxNeg = SampleDepthRGB(_InitialParticle, dxNeg);

				float depthdyPos = SampleDepthRGB(_InitialParticle, dyPos);
				float depthdyNeg = SampleDepthRGB(_InitialParticle, dyNeg);

				// Use the closer one (Unity is white-close black-far so it's a < here, to get the whiter one)
				// pick combination with smallest depth difference
				float3 P0 = reconstructPosition(uv, depth);

				float3 PR = reconstructPosition(uv + ux, depthdxPos);
				float3 PU = reconstructPosition(uv + uy, depthdyPos);

				float3 PL = reconstructPosition(uv - ux, depthdxNeg);
				float3 PD = reconstructPosition(uv - uy, depthdyNeg);

				float3 vx = PR - P0;
				float3 vy = PU - P0;

				if (abs(depthdxPos - depth) > abs(depthdxNeg - depth)) {
					vx = P0 - PL;
				}

				if (abs(depthdyPos - depth) > abs(depthdyNeg - depth)) {
					vy = P0 - PD;
				}
	
				return normalize(cross(vx, vy));
			}


            fixed4 frag (v2f_img2 i) : SV_Target {
                float Depth = SampleDepthRGB(_InitialParticle, i.uv);
				float3 Normal = float3(0,0,0);

				float2 uvScreenSpace = ClipToScreen(i.uv); // Also known as Frag Coord in GLSL
				float offsetX = _InitialParticle_TexelSize.x;
				float offsetY = _InitialParticle_TexelSize.y;
				
				int sampleOffset = 6;
				float counter = 0.0;

				if(Depth > 0) {
					for(int j = -sampleOffset; j <= sampleOffset; j++) {
						for(int k = -sampleOffset; k <= sampleOffset; k++) {
							float2 offset = float2(j * _InitialParticle_TexelSize.x, k* _InitialParticle_TexelSize.y) * sampleSpread;
							
							float fd = SampleDepthRGB(_InitialParticle,  i.uv+ offset);
							
							if (fd != 0) {
								float ddFactor = DepthFalloff((Depth - fd), uDepthFalloff);
								Normal += deriveNormal(i.uv + offset, fd) * ddFactor;
								counter += ddFactor;
							}
						}

					}
					Normal = Normal / counter;
					Normal = normalize(Normal);
					Normal = (Normal+float3(1,1,1))/2;
					
					//float fd = SampleDepthRGB(_InitialParticle, i.uv);
					//Normal = deriveNormal(i.uv , fd);
					//Normal = normalize(Normal);
					//Normal = (Normal*0.5) + float3(0.5, 0.5, 0.5);
				}
				

				return float4(Normal, tex2D(_InitialParticle, i.uv).a);
            }
            ENDCG
        }
    }
}
