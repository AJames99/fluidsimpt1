Shader "Custom/CurvatureFlow"
{
    Properties
    {
		//uCFThreshold("CF Threshold", Range(0, 1)) = 1
		//uCFFactor("CF Factor", Range(0.01, 1)) = 0.01
		uCFThreshold("CF Threshold", Range(0, 0.5)) = 0.05
		uCFFactor("CF Factor", Range(0, 10)) = 0.0001
		uDepthFalloff("Depth Falloff", Range(0,50)) = 1
		// Good values for 60 samples:
		// CF Threshold: 0.263
		// CF Factor: 3.73
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        CGINCLUDE
		#include "../FloatEncodeExtension.cginc"
        #include "UnityCG.cginc"

		//#define ONEMINUSDEPTH

        sampler2D _InitialParticle;			//Contains the simple depth (RGB encoded, 24bit) and simple thickness (alpha)
		sampler2D _InitialParticle2nd;		// Ditto. For ping-pong blitting.
		float4 _InitialParticle_TexelSize;	// Can use this for both sampler2D/RTs because they're basically just duplicates of eachother
		float uProjectFovVertical;
		float uProjectFov;
		float uCFThreshold;
		float uCFFactor;
		float uDepthFalloff;

		float2 ClipToScreen(float2 uv) {return  uv * _InitialParticle_TexelSize.zw;	}
		float2 ScreenToClip(float2 uv) { return uv * _InitialParticle_TexelSize.xy; }

		float SampleDepthRGB(sampler2D samp, float2 uv) {
			#ifdef ONEMINUSDEPTH
			return ( 1.0 - DecodeFloatRGB(tex2D(samp, uv).rgb)) * _ProjectionParams.z;
			#else
			return DecodeFloatRGB(tex2D(samp, uv).rgb) * _ProjectionParams.z;
			#endif
		}

		float dz2(float x, float y, float2 offsetPositive) {
			if(x<=0 || y<=0 || x>=1 || y>=1) {
				return 0;
			}

			float v0 = SampleDepthRGB(_InitialParticle, float2(x, y) - offsetPositive);
			float v1 = SampleDepthRGB(_InitialParticle, float2(x, y));
			float v2 = SampleDepthRGB(_InitialParticle, float2(x, y) + offsetPositive);

			//if(abs(v0 - v1) > uDepthFalloff || abs(v2 - v1) > uDepthFalloff || abs(v2 - v0) > uDepthFalloff)
			//	return 0.0;
			float depthDeltaMax = max(max(abs(v0-v1), abs(v2-v1)) , abs(v2-v0));
			//float ddFactor = saturate(1 - (depthDeltaMax / uDepthFalloff));
			float ddFactor = DepthFalloff(depthDeltaMax, uDepthFalloff);

			if(v1 == 0) {
				return 0.0;
			} else if((v0 == 0 && v2 != 0)) {
				return (v2 - v1)*ddFactor;
			} else if((v2 == 0 && v0 != 0)) {
				return (v1 - v0)*ddFactor;
			}
				
			return ddFactor*(v2 - v0) / 2.0;
		}
			
		float dz2x(float x, float y) {
			return dz2(x,y, ScreenToClip(float2(1,0)));
		}

		float dz2y(float x, float y) {
			return dz2(x,y, ScreenToClip(float2(0,1)));
		}


		// TODO: This needs to be done ITERATIVELY
        fixed4 fragCurvatureFlow (v2f_img i, sampler2D renderTex) {

			_InitialParticle = renderTex;

			float2 texCoord = i.uv; // uv space, 0 to 1
			float2 viewSpaceCoord = ClipToScreen(i.uv);

			float Depth = SampleDepthRGB(_InitialParticle, texCoord);

			float OffsetX = _InitialParticle_TexelSize.x;
			float OffsetY = _InitialParticle_TexelSize.y;

			float2 dx = float2 (OffsetX, 0.0);
			float2 dy = float2 (0.0,OffsetY);

			if(Depth != 0) {
				float dz_x = dz2x(texCoord.x, texCoord.y);
				float dz_x0 = dz2x(texCoord.x - OffsetX, texCoord.y);
				float dz_x2 = dz2x(texCoord.x + OffsetX, texCoord.y);
				float dz2x2 = (dz_x2 - dz_x0) / 2.0;

				float dz_y = dz2y(texCoord.x, texCoord.y);
				float dz_y0 = dz2y(texCoord.x, texCoord.y - OffsetY);
				float dz_y2 = dz2y(texCoord.x, texCoord.y + OffsetY);
				float dz2y2 = (dz_y2 - dz_y0) / 2.0;

				//float depthDifference = (Ex + Ey) * uDepthFalloff;
				//float depthFactor = exp(-depthDifference * depthDifference);
				//CF *= depthFactor;

				// In the paper this is view WIDTH AND HEIGHT over focal length, not the coordinate of the frag!!!!!!!
				//float Cx = 2 / (viewSpaceCoord.x * tan(uProjectFov / 2)); // kinda works? artefacty
				//float Cy = 2 / (viewSpaceCoord.y * tan(uProjectFovVertical / 2));
				float Cx = 2 / (_InitialParticle_TexelSize.z * tan(uProjectFov / 2)); // kinda works? uniform artefacting at least..
				float Cy = 2 / (_InitialParticle_TexelSize.w * tan(uProjectFovVertical / 2));

				//float D = (Cy*Cy * dz_x*dz_x) + (Cx*Cx * dz_y*dz_y) + (Cx*Cx*Cy*Cy*Depth*Depth);			// eq. 5
				float D = Cy * Cy * dz_x * dz_x + Cx * Cx * dz_y * dz_y + Cx * Cx * Cy * Cy * Depth * Depth;

				float inv_D32 = 1.0 / pow(D, 1.5);
		 
				
				float kx = 4.0 / (_InitialParticle_TexelSize.z * _InitialParticle_TexelSize.z);
				float ky = 4.0 / (_InitialParticle_TexelSize.w * _InitialParticle_TexelSize.w);

				float dD_x = ky * pow(viewSpaceCoord.y, -2.0) * 2 * dz_x * dz2x2  +
								kx * dz_y * dz_y * (-2) * pow(viewSpaceCoord.x, -3.0) +
								2 * ky * pow(viewSpaceCoord.y, -2.0) * kx * (-1 * pow(viewSpaceCoord.x, -3) * Depth * Depth + pow(viewSpaceCoord.x, -2) * Depth * dz_x);

				float dD_y = ky * (-2) * pow(viewSpaceCoord.y, -3) * dz_x * dz_x +
								kx * pow(viewSpaceCoord.x, -2) * 2 * dz_y * dz2y2 +
								2 * kx * ky * pow(viewSpaceCoord.x, -2) * (-1 * pow(viewSpaceCoord.y, -3) * Depth * Depth + pow(viewSpaceCoord.y, -2) * Depth * dz_y);	
				

				/*
				float zdxpyp = SampleDepthRGB (_InitialParticle,texCoord + dx + dy);
				float zdxnyn = SampleDepthRGB (_InitialParticle, texCoord - dx - dy);
				float zdxpyn = SampleDepthRGB(_InitialParticle, texCoord + dx - dy);
				float zdxnyp = SampleDepthRGB(_InitialParticle, texCoord - dx + dy);
				float zdxy = (zdxpyp + zdxnyn - zdxpyn - zdxnyp) / 4.0f;


				// Derivatives of said term
				float dD_x = Cy * Cy * 2.0f * dz_x * dz2x2 + Cx * Cx * 2.0f * dz_y * zdxy + Cx * Cx * Cy * Cy * 2.0f * Depth * dz_x;
				float dD_y = Cy * Cy * 2.0f * dz_x * zdxy + Cx * Cx * 2.0f * dz_y * dz2y2 + Cx * Cx * Cy * Cy * 2.0f * Depth * dz_y;
				*/

				float Ex = 0.5 * dz_x * dD_x - dz2x2 * D;
				float Ey = 0.5 * dz_y * dD_y - dz2y2 * D;

				float CF = (Cy * Ex + Cx * Ey) * inv_D32 / 2;

				/*CF = clamp(CF, -uCFThreshold, uCFThreshold);
				Depth = Depth - CF * uCFFactor; 
				Depth = max(Depth, 0.01);  // May need to be inverted?*/
				
				CF = CF > uCFThreshold ? uCFThreshold : CF;
				CF = CF < -uCFThreshold ? -uCFThreshold : CF;

				Depth = Depth - (CF * uCFFactor);
				
			}

			return float4(EncodeFloatRGB(Depth * _ProjectionParams.w), tex2D(_InitialParticle, i.uv).a);
        }

		fixed4 fragCurvatureFlow1st(v2f_img i) : SV_Target {
			return fragCurvatureFlow(i, _InitialParticle);
		}
		fixed4 fragCurvatureFlow2nd(v2f_img i) : SV_Target {
			return fragCurvatureFlow(i, _InitialParticle2nd);
		}

        ENDCG

		// Pass 0
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment fragCurvatureFlow1st
			ENDCG
		}
		// Pass 1
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment fragCurvatureFlow2nd
			ENDCG
		}
	}
}
