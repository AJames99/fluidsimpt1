// Encodes a 0 to 1 float as an 8bit/channel float3 
inline float3 EncodeFloatRGB(float v) {
	float3 kEncodeMul = float3(1.0, 255.0, 65025.0);
	float kEncodeBit = 1.0 / 255.0;
	float3 enc = kEncodeMul * v;
	enc = frac(enc);
	//enc.x -= enc.y * kEncodeBit; // RG...
	enc.xy -= enc.yz * kEncodeBit; // I think this is how RGB would work
	//enc -= enc.yzww * kEncodeBit; // RGBA...
	return enc;
}

inline float DecodeFloatRGB(float3 rgb) {
	float3 kDecodeDot = float3(1.0, 1.0/255.0, 1.0/65025.0);
	return dot(rgb, kDecodeDot);
}

//#define DEPTHMETHODNEW

inline float DepthFalloff(float depthDelta, float falloffFactor) {
#ifdef DEPTHMETHODNEW
	return saturate(1 - (depthDelta/falloffFactor));
#else
	depthDelta /= falloffFactor;
	return exp(-depthDelta * depthDelta);
#endif
}