Shader "Custom/ComposeRTT" {
	Properties {
		_MainTex("", 2D) = "white" {}
		_Shininess("Shininess", Float) = 0.5
		_FluidColour("Fluid Colour", Color) = (1.0, 0.0, 0.0, 0.0)
		_Opacity("Opacity", Range(0.0, 1.0)) = 1.0
		_ReflectanceNrmIncidence("_ReflectanceNrmIncidence", Range(0.0, 1.0)) = 0.01
		_ScatterColour("Scatter Colour", Color) = (0.05, 0.05, 0.10, 0.0)
		_ScatteringCoefficient("Scattering Coefficient", Float) = 0.11
		_ExtinctionValues("Colour Extinction", Vector) = (0.46, 0.09, 0.06, 0.0)
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass {


			ZWrite Off
			ZTest Off

			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM
			// Use the default fullscreen quad vertex shader
			#pragma vertex vert_img
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Lighting.cginc"
			#include "UnityLightingCommon.cginc"
			#include "FloatEncodeExtension.cginc"
			#include "UnityShaderVariables.cginc"

			sampler2D _MainTex;
			float4 _MainTex_TexelSize;
			sampler2D _FrameBufferTex;
			float _LerpDebug;

			float uProjectFovVertical;
			float uProjectFov;
			float3 _ExtinctionValues;
			float _ScatteringCoefficient;
			float3 _ScatterColour;
			float3 _FluidColour;

			float _Roughness;
			float _Shininess;
			float _ReflectanceNrmIncidence;
			float _Opacity;

			CBUFFER_START(LightingBuffer)
			float4 _DirectionalLightInfo;
			UNITY_DECLARE_TEXCUBE(_MainProbeTexture);
			CBUFFER_END

			float4x4 worldToCameraMatrix; // Likely to change every frame; moved outside CBUFFER.

			struct Light {
				float3 direction;
				float brightness;
			};

			Light GetDirectionalLight() {
				Light light;
				light.direction = _DirectionalLightInfo.xyz;
				light.brightness = _DirectionalLightInfo.w;
				return light;
			}

			float3 reconstructPosition(in float2 uv, in float z, in float4x4 InvVP) {
				float x = uv.x * 2.0f - 1.0f;
				float y = (1.0 - uv.y) * 2.0f - 1.0f;
				float4 position_s = float4(x, y, z, 1.0f);
				float4 position_v = mul(InvVP, position_s);
				return position_v.xyz / position_v.w;
			}

			float3 uvToEye(float2 UV, float depth) {
				return float3(UV * _MainTex_TexelSize.zw, 1.0 - depth);
			}

			float3 getEyePos(sampler2D depthTex, float2 screenspaceCoords) {
				return float3(screenspaceCoords, 1.0 - DecodeFloatRGB(tex2D(depthTex, screenspaceCoords * _MainTex_TexelSize.xy).rgb));
			}

			float4 frag(v2f_img i) : SV_Target {

				float4 fluidBuffer = tex2D(_MainTex, i.uv);
				fixed4 frameBuffer = tex2D(_FrameBufferTex, i.uv);
				

				//return (fluidBuffer);

				float2 angleSpacePos = i.uv -float2(0.5, 0.5);
				//float3 viewDirViewSpace = normalize(float3(tan(uProjectFov*angleSpacePos.x), tan(uProjectFovVertical*angleSpacePos.y), 1.0)); // wrong?
				float3 viewDirViewSpace = normalize(float3(angleSpacePos.x * tan(uProjectFov/2.0)/0.5, angleSpacePos.y * tan(uProjectFovVertical/2.0)/0.5, 1.0));

				float thicknessRaw = fluidBuffer.a;
				float thickness = saturate(_Opacity + _Opacity*thicknessRaw);
				float3 normal = (fluidBuffer.rgb * 2.0) - float3(1.0,1.0,1.0);
				normal = normalize(normal);
				//return float4((normal + 1)/2, 0.0);

				Light mainLight = GetDirectionalLight(); // Old
				float3 lightDirViewSpace = mul(worldToCameraMatrix, float4(mainLight.direction, 0)).xyz; // 0 for directions 1 for positions?

				float cosAngleIncidence = saturate(dot(normal, lightDirViewSpace));	// Lambertian term
				float3 halfAngle = normalize(lightDirViewSpace + viewDirViewSpace);
				float blinnTerm = saturate(dot(normal, halfAngle));

				//blinnTerm = cosAngleIncidence != 0.0 ? blinnTerm : 0.0;
				blinnTerm = pow(blinnTerm, _Shininess);

				// Fresnel via Shlick's Approximation
				float cosTheta = dot(normal, viewDirViewSpace);
				float fresnelFactor = _ReflectanceNrmIncidence + (1-_ReflectanceNrmIncidence)*pow(1.0-cosTheta,5);
				//return float4(fresnelFactor,fresnelFactor,fresnelFactor,1.0);
				//half MIP = PerceptualRoughnessToMipmapLevel( 0.3 );
				float3 viewDirWorldSpace = mul(unity_CameraToWorld, viewDirViewSpace);
				float3 normalWorldSpace = mul(unity_CameraToWorld, normal);
				float3 reflectedDir = reflect(viewDirWorldSpace, normalWorldSpace);
				//return float4(reflectedDir, 1.0);
				float4 val = UNITY_SAMPLE_TEXCUBE(_MainProbeTexture, reflectedDir);
                //float3 sampledCube = DecodeHDR(val, unity_SpecCube0_HDR);
				//float3 sampledCube = DecodeHDR(val, _MainProbeTexture_HDR);
				
				float3 sampledCube = val.xyz;
				
				//return float4(sampledCube,1);

				const float3 specularColour = float3(1.0, 1.0, 1.0);
				float3 diffuseShadedColour = (_FluidColour * cosAngleIncidence) + (_FluidColour * 0.05);
				float3 specularShadedColour = (specularColour * blinnTerm);
				
				const float refractFac = 25.0;
				float4 colRefracted = tex2D(_FrameBufferTex, i.uv + (normal * thickness / refractFac));
				//////
				float3 C_refracted = colRefracted * exp(-_ExtinctionValues * thicknessRaw);
				float3 b_actual = 1.0 - exp(-_ScatteringCoefficient * thicknessRaw);
				float3 C_final = lerp(C_refracted, _ScatterColour, b_actual) * saturate(cosAngleIncidence + 0.35);
				//////

				//float3 outputColour = lerp((colRefracted.rgb+colRefracted.rgb*diffuseShadedColour)/2, diffuseShadedColour, thickness) + specularShadedColour + (sampledCube * fresnelFactor);
				float3 outputColour = lerp(C_final, diffuseShadedColour, _Opacity) + specularShadedColour + (sampledCube * fresnelFactor);
				//return float4(outputColour, 1.0);

				// If the thickness is renderer irrespective of depth testing, then we can't use it in the final lerp!
				/*return float4(
					lerp(frameBuffer.rgb, outputColour, (ceil(thicknessRaw))),
				1.0);*/
				return float4(
					lerp(frameBuffer.rgb, outputColour, (ceil(fluidBuffer.r))),
				1.0);
			}
		ENDCG
	}
	}
}