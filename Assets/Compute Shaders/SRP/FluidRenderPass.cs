//#define DOWNSAMPLE

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

class FluidRenderPass : ScriptableRenderPass {
	// used to label this pass in Unity's Frame Debug utility
	string profilerTag;

	Material fluidDrawMaterialThickness, fluidDrawMaterialNrm, compositeMaterial, gaussBlurMat, particleCentredBlurMat, curvatureFlowMaterial, averageNormalMaterial, normalGenerationMaterial;
	RenderTargetIdentifier cameraColorTargetIdent;
    RenderTargetIdentifier cameraDepthTargetIdent;

    RenderTargetHandle fluidIntermediateTexture, frameBufferTexture, tempBuffer;//, initialParticleBuffer;
	RenderTargetHandle depthBuffer;

	const string frameBufferShaderProperty = "_FrameBufferTex";
	//const string depthTextureShaderProperty = "_ParticleDepth";
	//const string depthTextureBlurXShaderProperty = "_BlurOutput";

	public Bounds drawBounds;
	public ComputeBuffer argsBuffer;
	public Matrix4x4 transformMat;
	
	uint curvatureSmoothingIterations = 60;

	static int lightingBufferID = Shader.PropertyToID("_DirectionalLightInfo");
	static int probeTextureID = Shader.PropertyToID("_MainProbeTexture");
	public FluidRenderFeature.LightingInformation lightingInformation;

	public FluidRenderPass(string profilerTag, RenderPassEvent renderPassEvent, Material fluidDrawMaterialThickness, 
		Material fluidDrawMaterialNormal, Material compositeMaterial, Material gaussBlurMat, Material particleCentredBlurMat, 
		Material curvatureFlowMaterial, Material averageNormalMaterial, Material normalGenerationMaterial,
		ComputeBuffer argsBuffer, Transform transform, uint curvatureSmoothingIterations) {

		this.profilerTag = profilerTag;
		this.renderPassEvent = renderPassEvent;
		this.fluidDrawMaterialThickness = fluidDrawMaterialThickness;
		fluidDrawMaterialNrm = fluidDrawMaterialNormal;
		this.compositeMaterial = compositeMaterial;
		this.argsBuffer = argsBuffer;
		this.gaussBlurMat = gaussBlurMat;
		this.particleCentredBlurMat = particleCentredBlurMat;
		this.curvatureFlowMaterial = curvatureFlowMaterial;
		this.averageNormalMaterial = averageNormalMaterial;
		this.normalGenerationMaterial = normalGenerationMaterial;

		if (transform) {
			transformMat = new Matrix4x4(); 
			transformMat.SetTRS(transform.position, transform.rotation, transform.lossyScale);
		}
		//this.curvatureSmoothingIterations = 0;// curvatureSmoothingIterations;
		this.curvatureSmoothingIterations = curvatureSmoothingIterations;

		frameBufferTexture.Init(frameBufferShaderProperty);
		tempBuffer.Init("_InitialParticle");
		fluidIntermediateTexture.Init("_InitialParticle2nd");
        depthBuffer.Init("_DepthBuff");


    }

	// This isn't part of the ScriptableRenderPass class and is our own addition.
	// For this custom pass we need the camera's color target, so that gets passed in.
	public void Setup(RenderTargetIdentifier cameraColorTargetIdent, RenderTargetIdentifier cameraDepthTargetIdent) {
		this.cameraColorTargetIdent = cameraColorTargetIdent;
        //this.cameraDepthTargetIdent = cameraDepthTargetIdent;
    }

    RenderTargetIdentifier depthAttachmentId;

    // called each frame before Execute, use it to set up things the pass will need
    public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor) {
		RenderTextureDescriptor rtDescriptor = new RenderTextureDescriptor(cameraTextureDescriptor.width, cameraTextureDescriptor.height, RenderTextureFormat.ARGB32);
		rtDescriptor.depthBufferBits = cameraTextureDescriptor.depthBufferBits;
		// Create a temporary render texture that matches the camera
		cmd.GetTemporaryRT(fluidIntermediateTexture.id, rtDescriptor);
		cmd.GetTemporaryRT(frameBufferTexture.id, rtDescriptor);
		cmd.GetTemporaryRT(tempBuffer.id, rtDescriptor);
        //cmd.GetTemporaryRT(initialParticleBuffer.id, rtDescriptor);

        cmd.GetTemporaryRT(depthBuffer.id, cameraTextureDescriptor.width, cameraTextureDescriptor.height, cameraTextureDescriptor.depthBufferBits, FilterMode.Point, RenderTextureFormat.Depth  );

         depthAttachmentId = Shader.PropertyToID("_CameraDepthAttachment");
        cmd.CopyTexture(depthAttachmentId, depthBuffer.id);

        ////cmd.GetTemporaryRT(depthBuffer.id, new RenderTextureDescriptor(cameraTextureDescriptor.width, cameraTextureDescriptor.height, RenderTextureFormat.Depth), FilterMode.Point,);
    }

    // Execute is called for every eligible camera every frame. It's not called at the moment that
    // rendering is actually taking place, so don't directly execute rendering commands here.
    // Instead use the methods on ScriptableRenderContext to set up instructions.
    // RenderingData provides a bunch of (not very well documented) information about the scene
    // and what's being rendered.
    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData) {
		// fetch a command buffer to use
		CommandBuffer cmd = CommandBufferPool.Get(profilerTag);
		cmd.Clear();



        cmd.SetGlobalFloat("uProjectFov", Camera.main.fieldOfView * Mathf.Deg2Rad);
		cmd.SetGlobalFloat("uProjectFovVertical", Camera.HorizontalToVerticalFieldOfView(Camera.main.fieldOfView * Mathf.Deg2Rad, Camera.main.aspect));

        cmd.SetRenderTarget(tempBuffer.Identifier(), depthBuffer.Identifier()); // "_InitialParticle"

        cmd.ClearRenderTarget(false, true, Color.clear);

        cmd.DrawProceduralIndirect(transformMat, fluidDrawMaterialNrm, 0, MeshTopology.Points, argsBuffer, 0);			// Write depth to _InitialParticle

        cmd.DrawProceduralIndirect(transformMat, fluidDrawMaterialThickness, 0, MeshTopology.Points, argsBuffer, 0);    // Write thickness to _InitialParticle

		// "_InitialParticle" now has initial particle data pass
		uint smoothIterPairs = (uint) Mathf.CeilToInt(curvatureSmoothingIterations / 2.0f);
		for (int smoothIter = 0; smoothIter < smoothIterPairs; smoothIter++) {
			cmd.Blit(tempBuffer.Identifier(), fluidIntermediateTexture.Identifier(), curvatureFlowMaterial, 0);
			cmd.Blit(fluidIntermediateTexture.Identifier(), tempBuffer.Identifier(), curvatureFlowMaterial, 1);
		}
		
		cmd.SetGlobalMatrix("_InverseView", Camera.main.cameraToWorldMatrix);
		cmd.SetGlobalMatrix("worldToCameraMatrix", Camera.main.worldToCameraMatrix);

		cmd.Blit(tempBuffer.Identifier(), fluidIntermediateTexture.Identifier(), averageNormalMaterial);          // Store avg. normals in frameBufferTex

		// Copy camera colour buffer to the framebuffer // frame buffer -> _FrameBufferTex
		cmd.Blit(cameraColorTargetIdent, frameBufferTexture.Identifier());

		// Blit the fluid buffer and the framebuffer onto the camera colour buffer using the compositor shader
		if (lightingInformation.isDirty)
        {
			cmd.SetGlobalVector(lightingBufferID, lightingInformation.mainLightInfo);
			cmd.SetGlobalTexture(probeTextureID, lightingInformation.mainProbeTexture);
			//Debug.Log("mainLightInfo" + lightingInformation.mainLightInfo);
			lightingInformation.SetDirty(false);
		}

		const float _IORFluid = 1.333f; // Water
		//const float _IORFluid = 1.47f; // Olive Oil
		const float _IORAir = 1.000293f;
		const float _ReflectanceNrmIncidence = ((_IORFluid - _IORAir) / (_IORFluid + _IORAir)) * ((_IORFluid - _IORAir) / (_IORFluid + _IORAir));
		//compositeMaterial.SetFloat("_ReflectanceNrmIncidence", _ReflectanceNrmIncidence);

		cmd.Blit(fluidIntermediateTexture.Identifier(), cameraColorTargetIdent, compositeMaterial);



        // Don't forget to tell ScriptableRenderContext to actually execute the commands
        context.ExecuteCommandBuffer(cmd);

		// tidy up after ourselves
		cmd.Clear();
		CommandBufferPool.Release(cmd);
	}

	// called after Execute, use it to clean up anything allocated in Configure
	public override void FrameCleanup(CommandBuffer cmd) {
		cmd.ReleaseTemporaryRT(fluidIntermediateTexture.id);
		cmd.ReleaseTemporaryRT(frameBufferTexture.id);
		cmd.ReleaseTemporaryRT(tempBuffer.id);
		//cmd.ReleaseTemporaryRT(initialParticleBuffer.id);
	}
}