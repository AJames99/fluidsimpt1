using UnityEngine;
using UnityEngine.Rendering.Universal;

public class FluidRenderFeature : ScriptableRendererFeature {
	[System.Serializable]
	public class FeatureSettings {
		// we're free to put whatever we want here, public fields will be exposed in the inspector
		public bool IsEnabled = true;
		public RenderPassEvent WhenToInsert = RenderPassEvent.AfterRendering;
		public uint curvatureSmoothingIterations;
		public Material materialParticleDrawThickness;
		public Material materialParticleDrawNrm;
		public Material compositeMaterial;
		public Material gaussBlurMat;
		public Material particleCentredBlurMat;
		public Material curvatureFlowMaterial;
		public Material averageNormalMaterial;
		public Material normalGenerationMaterial;
	}

	// MUST be named "settings" (lowercase) to be shown in the Render Features inspector
	public FeatureSettings settings = new FeatureSettings();

	[HideInInspector] public ComputeBuffer argsBuffer;
	[HideInInspector] public Transform transform;

	FluidRenderPass fluidRenderPass;

	public class LightingInformation {
		public Vector4 mainLightInfo = Vector4.zero;
		public bool isDirty; // Has the data changed?
		public ReflectionProbe mainProbe;
		int hashCode = 0;
		public Vector4 mainLight {
			get {
				return mainLightInfo;
			}
		}
		public Texture mainProbeTexture {
			get {
				return mainProbe.bakedTexture;
			}
		}
		public void SetMainLight(Vector3 direction, float brightness) {
			mainLightInfo = direction.Copy();
			mainLightInfo.w = brightness;
			int newHash = GetHashCode();
			isDirty = hashCode != newHash;
			hashCode = newHash;
		}
		public void SetDirty(bool dirty) {
			isDirty = dirty;
		}

		public override int GetHashCode() {
			// In case compiled with arithmetic checking.
			unchecked {
				int result = mainLightInfo.GetHashCode();
				// To add to this:
				// result = (result*397) ^ otherData.GetHashCode();
				// result = (result*397) ^ evenMoreData.GetHashCode();
				// (397 is chosen as it's a prime number and primes help to reduce hash collisions);
				return result;
			}
		}

		public override bool Equals(object obj) {
			LightingInformation other = obj as LightingInformation;
			return other.mainLightInfo.Equals(mainLightInfo);
		}
	}

	public LightingInformation lightingInfo = new LightingInformation();

	//Material gaussBlurMat;

	public override void Create() {
		fluidRenderPass = new FluidRenderPass(
		  "Fluid Render Pass",
		  settings.WhenToInsert,
		  settings.materialParticleDrawThickness,
		  settings.materialParticleDrawNrm,
		  settings.compositeMaterial,
		  settings.gaussBlurMat,
		  settings.particleCentredBlurMat,
		  settings.curvatureFlowMaterial,
		  settings.averageNormalMaterial,
		  settings.normalGenerationMaterial,
		  argsBuffer,
		  transform,
		  settings.curvatureSmoothingIterations
		);
		fluidRenderPass.lightingInformation = lightingInfo;
	}

	public void SetLights(Light[] lights) {
		lightingInfo.SetMainLight(-lights[0].transform.forward, lights[0].intensity);
	}

	// called every frame once per camera
	public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData) {
		if (!settings.IsEnabled) {
			// We can do nothing this frame if we want :)
			return;
		}

		// Gather up and pass any extra information our pass will need.
		// In this case we're getting the camera's color buffer target
		var cameraColorTargetIdent = renderer.cameraColorTarget;
        var cameraDepthTargetID = renderer.cameraDepthTarget;
		fluidRenderPass.Setup(cameraColorTargetIdent, cameraDepthTargetID);
         

		/*Shader gaussyShader = Shader.Find("Hidden/GaussianBlurFluid");
		gaussBlurMat = new Material(gaussyShader);
		gaussBlurMat.name = "Gauss Blur";
		gaussBlurMat.hideFlags = HideFlags.HideAndDontSave;*/

		// Ask the renderer to add our pass.
		// Could queue up multiple passes and/or pick passes to use
		renderer.EnqueuePass(fluidRenderPass);
	}


	// Primarily for allowing the fluid script to assign the particle buffer of the drawing material
	public void SetBuffer(string name, ComputeBuffer buffer) {
		settings.materialParticleDrawThickness.SetBuffer(name, buffer);
		settings.materialParticleDrawNrm.SetBuffer(name, buffer);
		settings.particleCentredBlurMat.SetBuffer(name, buffer);
	}

	public void SetReflectionProbe(ReflectionProbe probe) {
		fluidRenderPass.lightingInformation.mainProbe = probe;
		fluidRenderPass.lightingInformation.isDirty = true;
	}

	/*protected override void Dispose(bool disposing) {
		base.Dispose(disposing);
		DestroyImmediate(gaussBlurMat);
		gaussBlurMat = null;
	}*/
}