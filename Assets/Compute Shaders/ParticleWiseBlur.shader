Shader "Custom/ParticleCentredBlur"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_pSize("Particle Size",Range(1,64)) = 1

		depthFallOffSpiral("Depth falloff (Spiral)", Range(0.0, 40.0)) = 15.0
		blurSizeBaseSpiral("Blur size base (Spiral)", Range(0.0, 50.0)) = 5.0
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }

		Pass {
			ZWrite On
			ZTest LEqual
			Blend Off

			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment fragThick
			#pragma target 5.0	
			#include "FloatEncodeExtension.cginc"
			#include "UnityCG.cginc"

			#define PI 3.141592

			StructuredBuffer<float3> particlePositions; // Name must match that of the compute shader's variable.       
			fixed4 _Color;
			float _pSize;
			float _thicknessDivisor;
			//sampler2D _MainTex;
			//float4 _MainTex_TexelSize;
			sampler2D _InitialParticle;
			float4 _InitialParticle_TexelSize;
			float depthFallOffSpiral;
			float blurSizeBaseSpiral;

			struct v2g {
				float4 colour : COLOR;
				float4 position : SV_POSITION;
			};

			struct g2f {
				float4 colour : COLOR;
				float4 position : SV_POSITION;      // Screen space
				float2 uv : TEXCOORD0;
				float2 depthThick : TEXCOORD1;
			};

			v2g vert(uint id : SV_VertexID, uint inst : SV_InstanceID)
			{
				v2g o;

				o.position = float4(particlePositions[inst], 1.0);
				o.colour.rgb = _Color;
				o.colour.a = 1.0;
				return o;
			}

			// Maximum possible number of verts you plan to generate
			// ** Note: input in an array of 1 element (because we have a point stream)
			[maxvertexcount(4)]
			void geom(point v2g IN[1], inout TriangleStream<g2f> triStream) {
				g2f o;

				float halfS = 0.5 * _pSize;
				float4 worldPos = mul(unity_ObjectToWorld, IN[0].position);

				float3 look = _WorldSpaceCameraPos - worldPos;
				look.y = 0;
				look = normalize(look);
				//float3 up = float3(0, 1, 0);
				//float3 right = cross(up, look);
				float3 right = UNITY_MATRIX_IT_MV[0].xyz;
				float3 up = -UNITY_MATRIX_IT_MV[1].xyz;

				float4 v[4];
				v[0] = float4(worldPos + halfS * right - halfS * up, 1.0);
				v[1] = float4(worldPos + halfS * right + halfS * up, 1.0);
				v[2] = float4(worldPos - halfS * right - halfS * up, 1.0);
				v[3] = float4(worldPos - halfS * right + halfS * up, 1.0);

				//float4x4 vp = mul(UNITY_MATRIX_MVP, unity_WorldToObject);
				o.position = UnityObjectToClipPos(v[0]);
				o.uv = float2(1, 0);
				o.colour = float4(1.0, 1.0, 0.0, 1.0);
				o.depthThick = float2(0, 0);
				o.depthThick.x = o.position.z / o.position.w;
				triStream.Append(o);

				o.position = UnityObjectToClipPos(v[1]);
				o.uv = float2(1, 1);
				o.depthThick.x = o.position.z / o.position.w;
				triStream.Append(o);

				o.position = UnityObjectToClipPos(v[2]);
				o.uv = float2(0, 0);
				o.depthThick.x = o.position.z / o.position.w;
				triStream.Append(o);

				o.position = UnityObjectToClipPos(v[3]);
				o.uv = float2(0, 1);
				o.depthThick.x = o.position.z / o.position.w;
				triStream.Append(o);
			}

			#define TAU 6.28318530718

			float4 fragThick(g2f IN, out float depth : SV_Depth) : SV_Target {
				// Normal
				float3 N;
				N.xy = IN.uv * 2.0 - 1.0; // -0.5 to 0.5 range
				float r2 = dot(N.xy,N.xy);
				if (r2 > 1.0) discard;
				//if(r2 < 0.707106781) return tex2D(_InitialParticle, IN.position * _InitialParticle_TexelSize.xy); // sqrt 1/2; if within half the radius of the circle, don't blend.
				N.z = -sqrt(1.0 - r2);


				depth = (IN.position.z - (sin(N.z)*IN.position.z)) / IN.position.w;

				// N Describes the X, Y, Z-unsigned position of this pixel on the sphere relative to the particle.

				float effectiveSizeScreenSpace = min(_pSize * IN.depthThick.x * blurSizeBaseSpiral, _pSize); // todo replace number with sensible camera z-plane value

				const float sampleStepsAngular = 16.0;
				const float sampleRings = 8.0;

				float accumulatedDepth, sampleDepth, falloffFactor, depthDelta, radius = 0;
				int r = 0;
				float centreDepth = DecodeFloatRGB(tex2D(_InitialParticle, IN.position.xy  * _InitialParticle_TexelSize.xy));
				accumulatedDepth = 0;
				float weight = 0;

				float radiusStep = effectiveSizeScreenSpace / sampleRings;
				float angleStep = TAU / sampleStepsAngular;

				float2 cartesianPoint = N.xy; // Location relative to centre of particle, 0 to 1
				float startRadius = length(cartesianPoint);
				float startAngle = atan2(cartesianPoint.y, cartesianPoint.x);

				// Optimisation: if the depth at this pixel is already within the sphere that constitutes THIS particle, skip.

				float remainingRadius = 1 - startRadius;
				for(float rRelative = startRadius; rRelative < 1; rRelative += 1/sampleRings) {
					// rRelative is 0 to 1, remainingRadius is 0 to 1
					float2 sampleCoord = (float2(cos(startAngle), sin(startAngle)) * rRelative) * _InitialParticle_TexelSize.xy;
					accumulatedDepth += DecodeFloatRGB(tex2D(_InitialParticle, sampleCoord));
					weight += 1;
				}

				return float4(EncodeFloatRGB(accumulatedDepth / weight), 0.0);
				//return float4(startRadius < effectiveSizeScreenSpace/2,0.0,0.0,0.0);
			}

			ENDCG
		}
	}
	//added this
	FallBack "Diffuse"
}
