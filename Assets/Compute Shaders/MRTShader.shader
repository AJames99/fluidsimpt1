Shader "Custom/MRTShader" {
	Properties{
		_MainTex("", 2D) = "white" {}
		_SecondTex("", 2D) = "white" {}
		_ThirdTex("", 2D) = "white" {}
	}

		CGINCLUDE
		
		#include "UnityCG.cginc"

		sampler2D _MainTex;
		sampler2D _SecondTex;
		sampler2D _ThirdTex;

		// MRT =====================
		struct FragmentOutput {
			half4 dest0 : SV_Target0;
			half4 dest1 : SV_Target1;
		};

		FragmentOutput fragMrt(v2f_img i) : SV_Target{
			FragmentOutput o;
			o.dest0 = frac(i.uv.x * 10);
			o.dest1 = frac(i.uv.y * 10);
			return o;
		}

		// Simple compositor =======
		half4 fragCombine(v2f_img i) : SV_Target {
			half4 t1 = tex2D(_MainTex, i.uv);
			half4 t2 = tex2D(_SecondTex, i.uv);
			half4 t3 = tex2D(_ThirdTex, i.uv);
			//return half4(t1.r, t2.g, t3.b, 1.0);
			return half4(1,0,0,1);
		}
		ENDCG

		SubShader {
			Cull Off ZWrite Off ZTest Always
				// Pass 0
				Pass{
					CGPROGRAM
					#pragma vertex vert_img
					#pragma fragment fragMrt
					ENDCG
				}
				// Pass 1
				Pass {
					CGPROGRAM
					#pragma vertex vert_img
					#pragma fragment fragCombine
					ENDCG
				}
		}
}
