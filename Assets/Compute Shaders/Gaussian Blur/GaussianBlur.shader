Shader "Custom/BilateralBlurFluid"
{
	Properties {
		depthFalloff("Depth falloff", Range(1.0, 40.0)) = 15.0
		blurSizeBase("Blur size base", Range(0.0, 10.0)) = 5.0

		depthFalloffSpiral("Depth falloff (Spiral)", Range(1.0, 40.0)) = 15.0
		blurSizeBaseSpiral("Blur size base (Spiral)", Range(0.0, 15.0)) = 5.0
	}
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

		CGINCLUDE		
			#include "../FloatEncodeExtension.cginc"
            #include "UnityCG.cginc"

            sampler2D _MainTex;
			sampler2D _XBlurOutput;
			sampler2D _FrameBufferTex;
			float4 _MainTex_TexelSize; // Texture dimensions!!! xy are 1/w,1/h and zw are w,h
			sampler2D _InitialParticle;
			float4 _InitialParticle_TexelSize;

			float depthFalloff;
			float blurSizeBase;
			float depthFalloffSpiral;
			float blurSizeBaseSpiral;

			float tex2DFloatRGB(sampler2D samp, float2 uv) {
				return DecodeFloatRGB(tex2D(samp, uv).rgb) * _ProjectionParams.z;
			}

#ifdef ENCODEQUARTER
		    half4 fragQuarter(v2f_img i) : SV_Target {
				float4 d = _MainTex_TexelSize.xyxy * float4(1, 1, -1, -1);
				half2 s;
				s  = half2(DecodeFloatRGB(tex2D(_MainTex, i.uv + d.xy).rgb), tex2D(_MainTex, i.uv + d.xy).a);
				s += half2(DecodeFloatRGB(tex2D(_MainTex, i.uv + d.xw).rgb), tex2D(_MainTex, i.uv + d.xw).a);
				s += half2(DecodeFloatRGB(tex2D(_MainTex, i.uv + d.zy).rgb), tex2D(_MainTex, i.uv + d.zy).a);
				s += half2(DecodeFloatRGB(tex2D(_MainTex, i.uv + d.zw).rgb), tex2D(_MainTex, i.uv + d.zw).a);
				return float4(EncodeFloatRGB(s.x * 0.25), s.y * 0.25);
			}
#else
			half4 fragQuarter(v2f_img i) : SV_Target {
				float4 d = _MainTex_TexelSize.xyxy * float4(1, 1, -1, -1);
				half4 s;
				s  = tex2D(_MainTex, i.uv + d.xy);
				s += tex2D(_MainTex, i.uv + d.xw);
				s += tex2D(_MainTex, i.uv + d.zy);
				s += tex2D(_MainTex, i.uv + d.zw);
				return s * 0.25;
			}
#endif


			#define BLURRADIUSGAUSS 4

			#define KERNEL_N 9
			#define KERNEL_LOWER 4
			#define KERNEL_UPPER 5

			float4 fragGauss(v2f_img i, float2 unit, sampler2D depthMap) {
				float gaussKernel[] = { 0.000229, 0.005977, 0.060598, 0.241732, 0.382928, 0.241732, 0.060598, 0.005977, 0.000229 };
				
				float baseDepth = tex2DFloatRGB(_MainTex, i.uv); // Always read from unblurred depth for this part

				float sum = 0;
				float totalWeight = 0;
				for(int x = -KERNEL_LOWER; x < KERNEL_UPPER; x++) {
					// Multiply by baseDepth to scale the blur based on distance ("world space blur")
					//float depthSample = tex2DFloatRGB(depthMap, i.uv + (unit * BLURRADIUSGAUSS * x * baseDepth));
					float depthSample = tex2DFloatRGB(depthMap, i.uv + (unit * BLURRADIUSGAUSS * x * baseDepth));
					float depthSampleUnblurred = tex2DFloatRGB(depthMap, i.uv);

					// Difference in depth
					float delta = saturate(1.0 - (15.0 * abs(baseDepth - depthSample)));
					float oneMinusDelta = 1.0 - delta;

					// Divide by difference to weaken blur across strong gradients
					sum += depthSample * gaussKernel[x + KERNEL_LOWER] * delta;
					sum += depthSampleUnblurred * gaussKernel[x + KERNEL_LOWER] * oneMinusDelta;
					totalWeight += gaussKernel[x + KERNEL_LOWER];// * delta;
				}

				// Divide by totalWeight to normalise
				return float4(EncodeFloatRGB(sum / totalWeight), tex2D(_MainTex, i.uv).a);
			}

			float4 fragGaussX(v2f_img i) : SV_Target {
				return fragGauss(i, float2(_MainTex_TexelSize.x, 0.0), _MainTex); // Blur original depth
			}

			float4 fragGaussY(v2f_img i) : SV_Target {
				return fragGauss(i, float2(0.0, _MainTex_TexelSize.y), _XBlurOutput); // Blur output from X pass
			}

			float normpdf(in float x, in float sigma) {
				return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
			}

			/*float normpdf3(in float3 v, in float sigma) {
				return 0.39894*exp(-0.5*dot(v,v)/(sigma*sigma))/sigma;
			}*/

			float4 fragSmart(v2f_img i) : SV_Target {
				const int mSize = 11;
				const int kSize = 5;
				
				float kernel[11];
				float finalDepth = 0.0;
				
				// Create kernel
				float sigma = 7.0;
				float Z = 0.0;
				for(int j = 0; j <= kSize; ++j) {
					kernel[kSize + j] = kernel[kSize - j] = normpdf(float(j), sigma);
				}

				float centreDepth = tex2DFloatRGB(_MainTex, i.uv);
				float sdepth, ddelta, ffactor;
				float blurRadius = blurSizeBase * centreDepth;

				// Read out texels
				for(int x = -kSize; x <= kSize; ++x) {
					for(int y = -kSize; y <= kSize; ++y) {
						sdepth = tex2DFloatRGB(_MainTex, i.uv + float2(blurRadius*x*_MainTex_TexelSize.x, blurRadius*y*_MainTex_TexelSize.y));
						ddelta = (sdepth - centreDepth) * depthFalloff;
						ffactor = exp(-ddelta * ddelta);
						Z += kernel[kSize + y] * kernel[kSize + x] * ffactor;
						finalDepth += ffactor * (kernel[kSize + y] * kernel[kSize + x] * sdepth);
					}
				}

				return float4(EncodeFloatRGB(finalDepth / Z), tex2D(_MainTex, i.uv).a);
			}

			float4 fragSimple(v2f_img i) : SV_Target {
				const float TAU = 6.28318530718;
				float Directions = 16.0;
				float Quality = 8.0;
				//float Size = 8.0;
				
				//Pixel colour - depth here
				float depth;
				float centreDepth = depth = tex2DFloatRGB(_InitialParticle, i.uv);
				float2 Radius = (blurSizeBaseSpiral / _InitialParticle_TexelSize.zw) * saturate(centreDepth * depthFalloffSpiral);
				float totalWeight = 0; int counter = 0;
				float sampledDepth, ddelta, ffactor;
				for(float d = 0.0; d < TAU; d+=TAU/Directions) {
					for(float ii=1.0/Quality; ii <= 1.0; ii += 1.0/Quality) {
						sampledDepth = tex2DFloatRGB(_InitialParticle, i.uv + float2(cos(d), sin(d))*Radius*ii);

						ffactor = DepthFalloff(sampledDepth - centreDepth, depthFalloffSpiral);
						totalWeight += ffactor;
						counter++;
						depth += sampledDepth;
					}
				}
				depth /= (Quality * Directions);
				depth = lerp(centreDepth, depth, saturate(totalWeight / (float)counter));

				return float4(EncodeFloatRGB(depth * _ProjectionParams.w), tex2D(_InitialParticle, i.uv).a);
			}

            ENDCG

		//	Pass 0 - Gauss X
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
            #pragma fragment fragGaussX
			ENDCG
		}
		//	Pass 1 - Gauss Y
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
            #pragma fragment fragGaussY
			ENDCG
		}
		//	Pass 2 - Gauss Downsample
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
            #pragma fragment fragQuarter
			ENDCG
		}
		//	Pass 3 - ShaderToy thing
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
            #pragma fragment fragSimple
			ENDCG
		}
    }
}
