Shader "Custom/MacklinFluidVisGPU"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_pSize("Particle Size",Range(1,64)) = 1
		_thicknessDivisor("Thickness Divisor", Range(1, 8096)) = 1024
	}
	
	SubShader {
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }

		Pass {
			Blend One One //Additive
			ZWrite Off
			ZTest Off //was on

			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment fragThick
			#pragma target 5.0	
			#include "UnityCG.cginc"

			#define PI 3.141592


			struct Part {
				float3 position;
				uint id;
			};

				StructuredBuffer<Part> particlePositions;

			fixed4 _Color;
			float _pSize;
			float _thicknessDivisor;
			sampler2D _MainTex;

			struct v2g {
				float4 colour : COLOR;
				float4 position : SV_POSITION;
			};

			struct g2f {
				float4 colour : COLOR;
				float4 position : SV_POSITION;      // Screen space
				float2 uv : TEXCOORD0;
				float2 depthThick : TEXCOORD1;
			};

			v2g vert(uint id : SV_VertexID, uint inst : SV_InstanceID)
			{
				v2g o;

				o.position = float4(particlePositions[inst].position, 1.0);
				o.colour.rgb = _Color;
				o.colour.a = 1.0;
				return o;
			}

			// Maximum possible number of verts you plan to generate
			// ** Note: input in an array of 1 element (because we have a point stream)
			[maxvertexcount(4)]
			void geom(point v2g IN[1], inout TriangleStream<g2f> triStream) {
				g2f o;

				float halfS = 0.5 * _pSize;
				float4 worldPos = mul(unity_ObjectToWorld, IN[0].position);

				float3 look = _WorldSpaceCameraPos - worldPos;
				look.y = 0;
				look = normalize(look);
				//float3 up = float3(0, 1, 0);
				//float3 right = cross(up, look);
				float3 right = UNITY_MATRIX_IT_MV[0].xyz;
				float3 up = -UNITY_MATRIX_IT_MV[1].xyz;

				float4 v[4];
				v[0] = float4(worldPos + halfS * right - halfS * up, 1.0);
				v[1] = float4(worldPos + halfS * right + halfS * up, 1.0);
				v[2] = float4(worldPos - halfS * right - halfS * up, 1.0);
				v[3] = float4(worldPos - halfS * right + halfS * up, 1.0);

				//float4x4 vp = mul(UNITY_MATRIX_MVP, unity_WorldToObject);
				o.position = UnityObjectToClipPos(v[0]);
				o.uv = float2(1, 0);
				o.colour = float4(1.0, 1.0, 0.0, 1.0);
				o.depthThick = float2(0, 0);
				o.depthThick.x = o.position.z / o.position.w;
				triStream.Append(o);

				o.position = UnityObjectToClipPos(v[1]);
				o.uv = float2(1, 1);
				o.depthThick.x = o.position.z / o.position.w;
				triStream.Append(o);

				o.position = UnityObjectToClipPos(v[2]);
				o.uv = float2(0, 0);
				o.depthThick.x = o.position.z / o.position.w;
				triStream.Append(o);

				o.position = UnityObjectToClipPos(v[3]);
				o.uv = float2(0, 1);
				o.depthThick.x = o.position.z / o.position.w;
				triStream.Append(o);
			}

			float4 fragThick(g2f IN, out float depth : SV_Depth) : SV_Target
			{
				float3 N;
				N.xy = IN.uv * 2.0 - 1.0; // -0.5 to 0.5 range
				float r2 = dot(N.xy,N.xy);
				if (r2 > 1.0) discard;
				N.z = -sqrt(1.0 - r2); //changed to -

				//float3 depth = float3(IN.depthThick.x, IN.depthThick.x, IN.depthThick.x) * 100;
				//return float4(depth, 1.0);
				//return IN.colour;

				//float thickness = abs(N.z) * _pSize * 2;
				float thickness = sin(-N.z) * _pSize * 2;

				float led = 1.0 / (_ZBufferParams.z *IN.position.z + _ZBufferParams.w);
				led += (N.z*_pSize);
				depth = -(led*_ZBufferParams.w - 1) / (led *_ZBufferParams.z);

				return float4(0,0,0,thickness / _thicknessDivisor);
			}

			/*float4 fragNrm(g2f IN) : SV_Target
			{
				float3 N;
				N.xy = IN.uv * 2.0 - 1.0; // -0.5 to 0.5 range
				float r2 = dot(N.xy,N.xy);
				if (r2 > 1.0) discard;
				N.z = sqrt(1.0 - r2);
				return float4(N, 1.0);
			}*/

			ENDCG
		}
	}
	//added this
	FallBack "Diffuse"
}
