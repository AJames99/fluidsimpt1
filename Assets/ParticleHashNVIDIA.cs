﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FluidSimNVIDIA;

namespace Hashing {
    public class ParticleHashNVIDIA {
		// ushort can represent 0...65,535. More than enough for this application.

		// Maps points in space and particle IDs
		private Dictionary<Vector3, List<ushort>> hash = new Dictionary<Vector3, List<ushort>>();
		private List<ushort>[] idList; // Fixed array of semi-fixed Lists of IDs

		private float cellSize;
		private ushort maxParticlesPerCell, maxConcurrentCells;

		// Temp buffers/vars - TODO is this (keeping in global var space) good practice? I dont want to keep reallocating memory for these when they're used a bajillion times per frame...
		//private List<ushort> tempIDListBig;
		//private List<ushort> tempIDListSmall;
		//private Vector3 neighbourKey = new Vector3();
		//private Vector3 tempKey = new Vector3();

		/// <summary>
		/// Creates a 3D hash grid for Particle structs.
		/// </summary>
		/// <param name="cellSize"></param>: The size of each cell.
		/// <param name="maxParticlesPerCell"></param>: The maximum number of particles per cell. Hard limit.
		/// <param name="maxConcurrentCells"></param>: The maximum number of concurrent cells overall. Hard limit (for now).
		public ParticleHashNVIDIA(float cellSize, ushort maxParticlesPerCell, ushort maxConcurrentCells) {
			this.cellSize = cellSize;
			this.maxParticlesPerCell = maxParticlesPerCell;
			this.maxConcurrentCells = maxConcurrentCells;

			// TODO this might be overkill; adjust? Use a smaller list?
			//tempIDListBig = new List<ushort>(maxParticlesPerCell * 27);
			//tempIDListSmall = new List<ushort>(maxParticlesPerCell);

			// Preallocate the arrays of lists that are used for each cell's contents.
			idList = new List<ushort>[maxConcurrentCells];
			for (int i = 0; i < maxConcurrentCells; i++) {
				idList[i] = new List<ushort>(maxParticlesPerCell);
			}

			// Pre-declare the number of cells in the dictionary to preallocate the memory.
			hash = new Dictionary<Vector3, List<ushort>>(maxConcurrentCells);
		}

		public void InsertParticles(Particle[] particles) {

			// Clear the hash dict every time we update
			hash.Clear();
			for (int c = 0; c < idList.Length; c++) {
				idList[c].Clear();
			}

			int arrayCounter = 0;

			for (ushort i = 0; i < particles.Length; i++) {
				Vector3 tempKey = PositionToKey(particles[i].x);

				if (!hash.ContainsKey(tempKey)) {
					// Add a new entry to the dictionary with the space-key. In that entry, place the next available ushort list.
					hash.Add(tempKey, idList[arrayCounter]);
					// In that ushort list, set the first element to the particle in question's ID
					idList[arrayCounter].Add(particles[i].id);

					arrayCounter++;
				} else {
					// If the key already exists, just append us to the end.
					hash[tempKey].Add(particles[i].id);
				}
			}
		}

		/// <summary>
		/// Returns a pointer to the big temporary list IF any neighbours are found surrounding the given point. Otherwise, returns null.
		/// </summary>
		/// <returns>Big temp list of ushort IDs or null, depending.</returns>
		public List<ushort> NeighboursOfPoint(Vector3 point) {
			Vector3 tempKey = PositionToKey(point);
			Vector3 neighbourKey;
			bool foundAny = false;
			List<ushort> neighbours = new List<ushort>(maxParticlesPerCell * 27);
			List<ushort> inCell = new List<ushort>(maxParticlesPerCell);
			// Iterate in neighbouring cube shape
			for (int i = -1; i <= 1; i += 1) {
				for (int j = -1; j <= 1; j += 1) {
					for (int k = -1; k <= 1; k += 1) {
						// Set key to the neighbouring cell's key
						neighbourKey.x = tempKey.x + (i * cellSize);
						neighbourKey.y = tempKey.y + (j * cellSize);
						neighbourKey.z = tempKey.z + (k * cellSize);

						// If the hash table contains that key, get the pointer of its ID list
						if (hash.ContainsKey(neighbourKey))
							inCell = hash[neighbourKey];
						else
							continue;

						// If the hash table actually contains things, append them to the neighbours list
						if (inCell.Count > 0) {
							neighbours.AddRange(inCell);
							foundAny = true;
						}
					}
				}
			}

			if (foundAny)
				return neighbours;
			return null;
		}

		public Vector3 PositionToKey(Vector3 position) {
			return new Vector3(
				Mathf.Round(position.x / cellSize) * cellSize,
				Mathf.Round(position.y / cellSize) * cellSize,
				Mathf.Round(position.z / cellSize) * cellSize
			);
		}

		public void PositionToKeyPrealloc(Vector3 position, out Vector3 output) {
			output.x = Mathf.Round(position.x / cellSize) * cellSize;
			output.y = Mathf.Round(position.y / cellSize) * cellSize;
			output.z = Mathf.Round(position.z / cellSize) * cellSize;
		}
	}
}

namespace MacklinFluid {
	public class ParticleHash {
		// ushort can represent 0...65,535. More than enough for this application.

		// Maps points in space and particle IDs
		private Dictionary<Vector3, List<ushort>> hash = new Dictionary<Vector3, List<ushort>>();
		private List<ushort>[] idList; // Fixed array of semi-fixed Lists of IDs

		private float cellSize;
		private ushort maxParticlesPerCell, maxConcurrentCells;

		// Temp buffers/vars - TODO is this (keeping in global var space) good practice? I dont want to keep reallocating memory for these when they're used a bajillion times per frame...
		//private List<ushort> tempIDListBig;
		//private List<ushort> tempIDListSmall;
		//private Vector3 neighbourKey = new Vector3();
		//private Vector3 tempKey = new Vector3();

		/// <summary>
		/// Creates a 3D hash grid for Particle structs.
		/// </summary>
		/// <param name="cellSize"></param>: The size of each cell.
		/// <param name="maxParticlesPerCell"></param>: The maximum number of particles per cell. Hard limit.
		/// <param name="maxConcurrentCells"></param>: The maximum number of concurrent cells overall. Hard limit (for now).
		public ParticleHash(float cellSize, ushort maxParticlesPerCell, ushort maxConcurrentCells) {
			this.cellSize = cellSize;
			this.maxParticlesPerCell = maxParticlesPerCell;
			this.maxConcurrentCells = maxConcurrentCells;

			// TODO this might be overkill; adjust? Use a smaller list?
			//tempIDListBig = new List<ushort>(maxParticlesPerCell * 27);
			//tempIDListSmall = new List<ushort>(maxParticlesPerCell);

			// Preallocate the arrays of lists that are used for each cell's contents.
			idList = new List<ushort>[maxConcurrentCells];
			for (int i = 0; i < maxConcurrentCells; i++) {
				idList[i] = new List<ushort>(maxParticlesPerCell);
			}

			// Pre-declare the number of cells in the dictionary to preallocate the memory.
			hash = new Dictionary<Vector3, List<ushort>>(maxConcurrentCells);
		}

		public void InsertParticles(Particle[] particles) {

			// Clear the hash dict every time we update
			hash.Clear();
			for (int c = 0; c < idList.Length; c++) {
				idList[c].Clear();
			}

			int arrayCounter = 0;

			for (ushort i = 0; i < particles.Length; i++) {
				Vector3 tempKey = PositionToKey(particles[i].position);

				if (!hash.ContainsKey(tempKey)) {
					// Add a new entry to the dictionary with the space-key. In that entry, place the next available ushort list.
					hash.Add(tempKey, idList[arrayCounter]);
					// In that ushort list, set the first element to the particle in question's ID
					idList[arrayCounter].Add(particles[i].id);

					arrayCounter++;
				} else {
					// If the key already exists, just append us to the end.
					hash[tempKey].Add(particles[i].id);
				}
			}
		}

		/// <summary>
		/// Returns a pointer to the big temporary list IF any neighbours are found surrounding the given point. Otherwise, returns null.
		/// </summary>
		/// <returns>Big temp list of ushort IDs or null, depending.</returns>
		public List<ushort> NeighboursOfPoint(Vector3 point) {
			Vector3 tempKey = PositionToKey(point);
			Vector3 neighbourKey;
			bool foundAny = false;
			List<ushort> neighbours = new List<ushort>(maxParticlesPerCell * 27);
			List<ushort> inCell = new List<ushort>(maxParticlesPerCell);
			// Iterate in neighbouring cube shape
			for (int i = -1; i <= 1; i += 1) {
				for (int j = -1; j <= 1; j += 1) {
					for (int k = -1; k <= 1; k += 1) {
						// Set key to the neighbouring cell's key
						neighbourKey.x = tempKey.x + (i * cellSize);
						neighbourKey.y = tempKey.y + (j * cellSize);
						neighbourKey.z = tempKey.z + (k * cellSize);

						// If the hash table contains that key, get the pointer of its ID list
						if (hash.ContainsKey(neighbourKey))
							inCell = hash[neighbourKey];
						else
							continue;

						// If the hash table actually contains things, append them to the neighbours list
						if (inCell.Count > 0) {
							neighbours.AddRange(inCell);
							foundAny = true;
						}
					}
				}
			}

			if (foundAny)
				return neighbours;
			return null;
		}

		public Vector3 PositionToKey(Vector3 position) {
			return new Vector3(
				Mathf.Round(position.x / cellSize) * cellSize,
				Mathf.Round(position.y / cellSize) * cellSize,
				Mathf.Round(position.z / cellSize) * cellSize
			);
		}

		public void PositionToKeyPrealloc(Vector3 position, out Vector3 output) {
			output.x = Mathf.Round(position.x / cellSize) * cellSize;
			output.y = Mathf.Round(position.y / cellSize) * cellSize;
			output.z = Mathf.Round(position.z / cellSize) * cellSize;
		}
	}

	public class ParticleHashGPU {
		private Dictionary<Vector3, List<uint>> hash = new Dictionary<Vector3, List<uint>>();
		private List<uint>[] idList; // Fixed array of semi-fixed Lists of IDs

		private float cellSize;
		private int maxParticlesPerCell, maxConcurrentCells;
		public ParticleHashGPU(float cellSize, int maxParticlesPerCell, int maxConcurrentCells) {
			this.cellSize = cellSize;
			this.maxParticlesPerCell = maxParticlesPerCell;
			this.maxConcurrentCells = maxConcurrentCells;
			// Preallocate the arrays of lists that are used for each cell's contents.
			idList = new List<uint>[maxConcurrentCells];
			for (int i = 0; i < maxConcurrentCells; i++) {
				idList[i] = new List<uint>(maxParticlesPerCell);
			}

			// Pre-declare the number of cells in the dictionary to preallocate the memory.
			hash = new Dictionary<Vector3, List<uint>>(maxConcurrentCells);
		}

		public void InsertParticles(ParticleGPU[] particles, Vector3[] particlePositions) {

			// Clear the hash dict every time we update
			hash.Clear();
			for (int c = 0; c < idList.Length; c++) {
				idList[c].Clear();
			}

			int arrayCounter = 0;

			for (ushort i = 0; i < particles.Length; i++) {
				Vector3 tempKey = PositionToKey(particlePositions[i]);

				if (!hash.ContainsKey(tempKey)) {
					// Add a new entry to the dictionary with the space-key. In that entry, place the next available ushort list.
					hash.Add(tempKey, idList[arrayCounter]);
					// In that ushort list, set the first element to the particle in question's ID
					idList[arrayCounter].Add(particles[i].id);

					arrayCounter++;
				} else {
					// If the key already exists, just append us to the end.
					hash[tempKey].Add(particles[i].id);
				}
			}
		}

		public List<uint> NeighboursOfPoint(Vector3 point) {
			Vector3 tempKey = PositionToKey(point);
			Vector3 neighbourKey;
			bool foundAny = false;
			List<uint> neighbours = new List<uint>(maxParticlesPerCell * 27);
			List<uint> inCell = new List<uint>(maxParticlesPerCell);
			// Iterate in neighbouring cube shape
			for (int i = -1; i <= 1; i += 1) {
				for (int j = -1; j <= 1; j += 1) {
					for (int k = -1; k <= 1; k += 1) {
						// Set key to the neighbouring cell's key
						neighbourKey.x = tempKey.x + (i * cellSize);
						neighbourKey.y = tempKey.y + (j * cellSize);
						neighbourKey.z = tempKey.z + (k * cellSize);

						// If the hash table contains that key, get the pointer of its ID list
						if (hash.ContainsKey(neighbourKey))
							inCell = hash[neighbourKey];
						else
							continue;

						// If the hash table actually contains things, append them to the neighbours list
						if (inCell.Count > 0) {
							neighbours.AddRange(inCell);
							foundAny = true;
						}
					}
				}
			}

			if (foundAny)
				return neighbours;
			return null;
		}

		public Vector3 PositionToKey(Vector3 position) {
			return new Vector3(
				Mathf.Round(position.x / cellSize) * cellSize,
				Mathf.Round(position.y / cellSize) * cellSize,
				Mathf.Round(position.z / cellSize) * cellSize
			);
		}

		public void PositionToKeyPrealloc(Vector3 position, out Vector3 output) {
			output.x = Mathf.Round(position.x / cellSize) * cellSize;
			output.y = Mathf.Round(position.y / cellSize) * cellSize;
			output.z = Mathf.Round(position.z / cellSize) * cellSize;
		}
	}

	public class NeighbourhoodLookupCPU {
		Vector3[] points;
		float sqrRadius;
		uint[] neighbourhoodCounts;

		public NeighbourhoodLookupCPU(Vector3[] points, float radius) {
			this.points = points;
			sqrRadius = radius*radius;
			neighbourhoodCounts = new uint[points.Length];
		}

		public void SetPoints(Vector3[] points) {
			this.points = points;
		}

		public uint[,] GetNeighbourhoods() {
			uint[,] results = new uint[points.Length, Neighbourhood.MAXNEIGHBOURS];

			for (uint p = 0; p < points.Length; p++) {
				uint subIndex = 0;
				for (uint pOther = 0; subIndex < Neighbourhood.MAXNEIGHBOURS && pOther < points.Length; pOther++) {
					if ((points[p] - points[pOther]).sqrMagnitude <= sqrRadius) {
						results[p, subIndex++] = pOther;
						neighbourhoodCounts[p] = subIndex + 1;
					}
				}
			}

			return results;
		}

		public uint[] GetCounts() {
			return neighbourhoodCounts;
		}
	}
}
