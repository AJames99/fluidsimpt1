using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SimpleCameraOrbit : MonoBehaviour {

	public Vector3 orbitCentre = Vector3.zero;
	public float orbitSpeed = 4.0f;

	Vector2 _MoveDir;
	float _MoveDirVertical;

	public void OnOrbit(InputAction.CallbackContext context) {
		_MoveDir = context.ReadValue<Vector2>();
	}

	public void OnOrbitUpDown(InputAction.CallbackContext context) {
		_MoveDirVertical = context.ReadValue<float>();
	}

	// Update is called once per frame
	void Update() {
		transform.LookAt(orbitCentre);
		if (_MoveDir.sqrMagnitude != 0 || _MoveDirVertical != 0) {
			transform.position += (	(transform.forward * _MoveDir.y * orbitSpeed) + 
									(transform.right * _MoveDir.x * orbitSpeed) +
									(transform.up * _MoveDirVertical * orbitSpeed)
									) * Time.deltaTime;
		}
	}
}
