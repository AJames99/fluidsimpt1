﻿//Prepros
//#define DEBUGNEIGHBOUR
//#define COLLISIONDETECTION
//#define SPEEDLIMIT
#define SWIRLYGRAVITY
#define NOISYSPAWN

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using MacklinFluid;
// Based on http://mmacklin.com/pbf_sig_preprint.pdf

namespace MacklinFluid {
	public class MacklinFluidSim : MonoBehaviour {

		const ushort solverIterations = 5;//5;
		const int CUBE_SIZE = 10; //4x4x4
		int particleCount = CUBE_SIZE * CUBE_SIZE * CUBE_SIZE;
		const int PARTICLEABSOLUTELIMIT = 1000; // Cannot go higher than this.
		const float MASS = 1.0f; // Uniform mass across all particles.
		[Range(0.00019f, 0.001f)]
		[SerializeField]
		float DENSITY_RESTING = 0.00019f;
		const float RADIUS = 15f; //15 works well
		const float C = 0.01f; // XSPH Viscosity
		const float K = 0.1f;
		const float N = 4f;//4f;
		const float deltaQ = 0.2f * RADIUS;//0.2f * RADIUS;
		float wDeltaQ;
		const float epsilonLambda = 0.15f;


		//static float deltaT = 1f / 60f;//(1 / 60f) / solverIterations;
									   //static Vector3 Gravity = 9.81f * Vector3.down;
		static Vector3 Gravity = 9.81f * (Vector3.down + Vector3.right / 4 + Vector3.forward / 5).normalized; // Corner gravity
		const float BOUNDARYDIST = 150f;//50f; 50 is good for demoing 15fradius
		const int maxParticlesPerCell = 16;
		const float cellSizeHashgrid = RADIUS;//0.75f;
		Particle[] particles;
		ParticleHash hashTable;

		// Global memory alloc
		static List<ushort>[] neighbourhoods;
		//static List<float>[] neighbourhoodDistances;
		//static Particle p;

		// Incompressible flow simulated via Position Based Dynamics as detailed in the Macklin & Müller NVIDIA paper
		void Step(float deltaT) {
			//MacklinFluidSim.deltaT = deltaT;

			for (int i = 0; i < particleCount; i++) {
				// Apply forces
				particles[i].forces = Vector3.zero;
				particles[i].forces += /*particles[i].forces +*/ Gravity * 10;

#if COLLISIONDETECTION
				// Apply repulsion forces from colliders
				Vector3 dx;
				for (int c = 0; c < collidingObjects.Length; c++) {
					dx = particles[i].position - collidingObjects[c].position;
					float radi = collidingColliders[c].radius + RADIUS;

					Vector3 repForce = dx.normalized * Mathf.Max(radi - dx.magnitude, 0);
					float massRatio = (MASS / collidingObjects[c].mass);
					particles[i].forces += repForce / massRatio;
					collidingObjects[c].AddForce(repForce * massRatio, ForceMode.Acceleration);
				}
#endif

				particles[i].velocity += (particles[i].forces * deltaT);
				// predict positions		
				particles[i].newPosition += (particles[i].velocity * deltaT);

				EnforceBoundaries(particles[i]);
			}

			GenerateNeighbourhoods(true);

			ushort solves = 0;
			while (solves++ < solverIterations) {
				// DENSITY
				for (int i = 0; i < particleCount; i++) {
					float density = 0f;
					for (int n = 0; n < neighbourhoods[i].Count; n++) {
						density += MASS * Kernel.Poly6Kernel(particles[i].newPosition, particles[neighbourhoods[i][n]].newPosition);

						// Experimental: Collision depenetration
#if COLLISIONDETECTION
						Vector3 dx = (particles[i].newPosition - particles[neighbourhoods[i][n]].newPosition);
						float intersection = Mathf.Max((RADIUS * RADIUS * 2) - dx.sqrMagnitude, 0);
						particles[i].forces += dx * intersection;
#endif
					}

					float equationOfStateTerm = (density / DENSITY_RESTING) - 1f;

					Vector3 gradientVectorSum = Vector3.zero;
					float gradientMagnitudeSum = 0f;

					for (int n = 0; n < neighbourhoods[i].Count; n++) {
						Vector3 gradient = Kernel.SpikyGradient(particles[i].newPosition, particles[neighbourhoods[i][n]].newPosition) * (1f / DENSITY_RESTING);
						gradientVectorSum += gradient;
						gradientMagnitudeSum += gradient.sqrMagnitude;
					}
					gradientMagnitudeSum += gradientVectorSum.sqrMagnitude; // is this correct??
					particles[i].lambda = equationOfStateTerm / (gradientMagnitudeSum + epsilonLambda) * -1;
				}

				// PRESSURE
				for (int i = 0; i < particleCount; i++) {
					Vector3 deltaP = Vector3.zero;
					for (int n = 0; n < neighbourhoods[i].Count; n++) {

						Vector3 neighbourPosition = particles[neighbourhoods[i][n]].newPosition;

						float corr = Kernel.Poly6Kernel(particles[i].newPosition, neighbourPosition) / wDeltaQ;
						corr = Mathf.Pow(corr, N);
						float s_correction = K * corr * -1.0f;

						float lambdaCorrectionTerm = (particles[i].lambda + particles[neighbourhoods[i][n]].lambda + s_correction);
						Vector3 gradient = Kernel.SpikyGradient(particles[i].newPosition, neighbourPosition);
						Vector3 corrected_gradient = gradient * lambdaCorrectionTerm;
						deltaP += corrected_gradient;
					}
					particles[i].deltaPos = deltaP * (1.0f / DENSITY_RESTING);
				}

				for (int i = 0; i < particleCount; i++) {
					// update positions	using the deltaPosition	
					particles[i].newPosition = particles[i].newPosition + particles[i].deltaPos;

					// perform collision detection and response
					// in this implementation only for boundary condition
					EnforceBoundaries(particles[i]);
				}
			}

			for (int i = 0; i < particleCount; i++) {
				Vector3 direction = particles[i].newPosition - particles[i].position;
				Vector3 newVelocity = direction / deltaT;
				// This version limits speed based on radius size
#if SPEEDLIMIT
				particles[i].velocity = newVelocity.sqrMagnitude > (2*RADIUS*RADIUS) ? newVelocity.normalized*2*RADIUS : newVelocity;
#else
				particles[i].velocity = newVelocity;
#endif

				// apply vorticity confinement and xsph viscosity
				XSPHViscosity(particles[i], i);
				// update positions
				// .Copy() avoids sharing the same reference of position and newPosition of the Particle class 
				particles[i].position = particles[i].newPosition.Copy();
			}
		}

		void XSPHViscosity(Particle pi, int i) {
			Vector3 viscosity = Vector3.zero;
			for (int n = 0; n < neighbourhoods[i].Count; n++) {
				Vector3 relativeVel = pi.velocity - particles[neighbourhoods[i][n]].velocity;
				float poly6Value = Kernel.Poly6Kernel(pi.newPosition, particles[neighbourhoods[i][n]].newPosition);

				Vector3 addedGradientVel = relativeVel * poly6Value;
				viscosity += addedGradientVel;
			}
			pi.velocity += (viscosity * C);
		}

		void GenerateNeighbourhoods(bool drawConnections = false) {
			hashTable.InsertParticles(particles);
			for (int i = 0; i < particleCount; i++) {
				neighbourhoods[i] = new List<ushort>(
					hashTable.NeighboursOfPoint(particles[i].position)
				);
#if DEBUGNEIGHBOUR
				if (drawConnections) {
					foreach (ushort id in neighbourhoods[i]) {
						if (id > i)
							Debug.DrawLine(particles[i].position, particles[id].position, Color.red);
					}
				}
#endif
				//neighbourhoodDistances[i] = new List<float>(neighbourhoods[i].Count);
			}
		}

		Vector3 GetExternalForcesFor(Particle p) {
			return Gravity;
		}
		void EnforceBoundaries(Particle p) {
			// Newest method: Project x forward by v, and CLAMP both position and velocity by that.
			/*Vector3 overExtension = new Vector3();
			float projectPosAxis;
			for (int d = 0; d < 3; d++) {
				projectPosAxis = p.position[d] + (p.velocity[d] * deltaT);// - Mathf.Clamp(p.x[d], -BOUNDARYDIST, BOUNDARYDIST));
				overExtension[d] = projectPosAxis - Mathf.Clamp(projectPosAxis, -BOUNDARYDIST, BOUNDARYDIST);
			}
			p.position -= overExtension;
			p.velocity -= overExtension / deltaT;*/
			float correct = 0.001f;
			for (int d = 0; d < 3; d++) {
				if (p.newPosition[d] < -BOUNDARYDIST) {
					p.newPosition[d] = -BOUNDARYDIST + correct;
				}
				if (p.newPosition[d] > BOUNDARYDIST) {
					p.newPosition[d] = BOUNDARYDIST - correct;
				}
			}
		}

		void InitParticles() {
			particles = new Particle[particleCount];
			neighbourhoods = new List<ushort>[particleCount];

			
			hashTable = new ParticleHash(cellSizeHashgrid, maxParticlesPerCell, PARTICLEABSOLUTELIMIT);

			visualParticleMatrices = new Matrix4x4[PARTICLEABSOLUTELIMIT];
			/*
			visualParticles = new Transform[PARTICLEABSOLUTELIMIT];
			for (int k = 0; k < PARTICLEABSOLUTELIMIT; k++) {
				visualParticles[k] = Instantiate(visualPrefab, transform).transform;
				visualParticles[k].localPosition = Vector3.zero;
				visualParticles[k].localScale = Vector3.one * visualScale;

				// Disable unused particles for now.
				if (k >= particleCount)
					visualParticles[k].gameObject.SetActive(false);
			}*/

			//for (int i = 0; i < particleCount; i++) {
			ushort i = 0;
			for (int z = 0; z < CUBE_SIZE; z++)
				for (int y = 0; y < CUBE_SIZE; y++)
					for (int x = 0; x < CUBE_SIZE; x++) {
#if NOISYSPAWN
						particles[i] = new Particle(new Vector3(x, y, z) * RADIUS + (Random.insideUnitSphere), i++);
#else
						particles[i] = new Particle(new Vector3(x, y, z) * RADIUS, i++);
#endif
					}

			Kernel.SetKernel(RADIUS);
			wDeltaQ = Kernel.POLY6 * Mathf.Pow(((RADIUS * RADIUS) - (deltaQ * deltaQ)), 3);
		}

		// My blackened hatred for Unity cannot truly be expressed in any human tongue.
		void DrawVis() {
			/*foreach (Particle p in particles) {
				Debug.DrawLine(transform.position + p.x + Vector3.left, transform.position + p.x + Vector3.right);
				Debug.DrawLine(transform.position + p.x + Vector3.up, transform.position + p.x + Vector3.down);
				Debug.DrawLine(transform.position + p.x + Vector3.forward, transform.position + p.x + Vector3.back);
			}*/

			// Works, slow.
			/*for (int i = 0; i < particleCount; i++) {
				visualParticles[i].localPosition = particles[i].position;
			}*/

			for (int i = 0; i < particleCount; i++) {
				visualParticleMatrices[i].SetTRS(particles[i].position, Quaternion.identity, visualScaleV);
			}

			Graphics.DrawMeshInstanced(instanceMesh, 0, instanceMat, visualParticleMatrices, particleCount);
		}


		// For the (admittedly suboptimal and slow) visualisation of the particles, create a sphere at each point.
		// TODO when geometry shaders start behaving, move to that.
		[SerializeField]
		private GameObject visualPrefab;
		//private Transform[] visualParticles;
		private Matrix4x4[] visualParticleMatrices;
		[SerializeField]
		Mesh instanceMesh;
		[SerializeField]
		Material instanceMat;

		[SerializeField]
		private float visualScale = 0.1f;
		private Vector3 visualScaleV;
		[SerializeField]
		private Text textFieldDebug;

		Rigidbody[] collidingObjects;
		SphereCollider[] collidingColliders;

		private void Start() {
			InitParticles();

			visualScaleV = new Vector3(visualScale, visualScale, visualScale);

			//deltaT = Time.fixedDeltaTime;// / solverIterations;
			List<Rigidbody> rbs = new List<Rigidbody>(GetComponentsInChildren<Rigidbody>());
			List<SphereCollider> spheres = new List<SphereCollider>();
			foreach (Rigidbody rb in rbs) {
				if (rb.isKinematic) {
					rbs.Remove(rb);
				} else {
					spheres.Add(rb.GetComponent<SphereCollider>());
				}
			}
			collidingObjects = rbs.ToArray();
			collidingColliders = spheres.ToArray();

			Physics.gravity = Gravity;
		}

		private void FixedUpdate() {
#if SWIRLYGRAVITY
			Gravity = new Vector3(5 * Mathf.Cos(Time.fixedTime / 3), -3f, 5 * Mathf.Sin(Time.fixedTime / 3));
			Physics.gravity = Gravity;
#endif
			Step(Time.fixedDeltaTime);
		}

		private void Update() {
			DrawVis();
		}

		/*private void Update() {
			if (Input.GetKeyDown(KeyCode.G)) {
				for (int i = 0; i < particleCount; i++) {
					//particles[i].v += Random.insideUnitSphere;
					particles[i].v += (-particles[i].x).normalized + Random.insideUnitSphere / 2;
				}
			}
		}*/
	}
}